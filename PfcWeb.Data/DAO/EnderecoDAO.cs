﻿using PfcWeb.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using Microsoft.SqlServer;
using PfcWeb.Domain.Enum;
using PfcWeb.Data.Connection;

namespace PfcWeb.Data.DAO
{
    public class EnderecoDAO
    {
        public bool Cadastrar(Endereco endereco)
        {
            SqlTransaction sqlTransaction = null;

            try
            {
                Conexao objConexao = new Conexao();
                SqlCommand command = new SqlCommand();

                using (var connection = new SqlConnection(objConexao.getConexao()))
                {
                    command.Connection = connection;

                    connection.Open();

                    sqlTransaction = connection.BeginTransaction();

                    command.Transaction = sqlTransaction;

                    command.CommandText = @"INSERT INTO Endereco (Rua,
                                                                  Numero,
                                                                  Complemento,
                                                                  Bairro,
                                                                  CEP,
                                                                  Cidade,
                                                                  Estado)
                                                         VALUES (@Rua,
                                                                 @Numero,
                                                                 @Complemento,
                                                                 @Bairro,
                                                                 @CEP,
                                                                 @Cidade,
                                                                 @Estado)";

                    command.Parameters.AddWithValue("@Rua", endereco.Rua);
                    command.Parameters.AddWithValue("@Numero", endereco.Numero);
                    command.Parameters.AddWithValue("@Complemento", endereco.Complemento);
                    command.Parameters.AddWithValue("@Bairro", endereco.Bairro);
                    command.Parameters.AddWithValue("@CEP", endereco.CEP);
                    command.Parameters.AddWithValue("@Cidade", endereco.Cidade);
                    command.Parameters.AddWithValue("@Estado", endereco.Estado);

                    command.ExecuteNonQuery();

                    sqlTransaction.Commit();

                    connection.Close();

                    return true;
                }
            }
            catch (Exception)
            {
                sqlTransaction.Rollback();
                return false;
            }
        }

        public Endereco ListarSelecionado(int ID)
        {
            Endereco endereco = new Endereco();
            try
            {
                Conexao objConexao = new Conexao();
                SqlCommand command = new SqlCommand();

                using (var connection = new SqlConnection(objConexao.getConexao()))
                {
                    command.Connection = connection;

                    connection.Open();

                    command.CommandText = @"SELECT * FROM Endereco
                                                    WHERE ID = @ID;";

                    command.Parameters.AddWithValue("@ID", ID);

                    SqlDataReader dr = command.ExecuteReader();

                    dr.Read();

                    string estado;

                    endereco.ID = Convert.ToInt32(dr["ID"]);
                    endereco.Rua = dr["Rua"].ToString();
                    endereco.Numero = Convert.ToInt32(dr["Numero"]);
                    endereco.Complemento = dr["Complemento"].ToString(); ;
                    endereco.Bairro = dr["Bairro"].ToString();
                    endereco.CEP = Convert.ToInt32(dr["CEP"]);
                    endereco.Cidade = dr["Cidade"].ToString();
                    estado = dr["Estado"].ToString();

                    endereco.Estado = switchEstado(estado).Estado;

                    connection.Close();
                }
                return endereco;
            }
            catch (Exception)
            {
                return endereco = null;
            }
        }

        public Endereco ListarUltimo()
        {
            Endereco endereco = new Endereco();
            try
            {
                Conexao objConexao = new Conexao();
                SqlCommand command = new SqlCommand();

                using (var connection = new SqlConnection(objConexao.getConexao()))
                {
                    command.Connection = connection;

                    connection.Open();

                    command.CommandText = @"SELECT TOP 1 * FROM Endereco
                                                       ORDER BY ID DESC;";

                    SqlDataReader dr = command.ExecuteReader();

                    string estado;

                    while (dr.Read())
                    {
                        endereco.ID = Convert.ToInt32(dr["ID"]);
                        endereco.Rua = dr["Rua"].ToString();
                        endereco.Numero = Convert.ToInt32(dr["Numero"]);
                        endereco.Complemento = dr["Complemento"].ToString();
                        endereco.Bairro = dr["Bairro"].ToString();
                        endereco.CEP = Convert.ToInt32(dr["CEP"]);
                        endereco.Cidade = dr["Cidade"].ToString();
                        estado = dr["Estado"].ToString();

                        endereco.Estado = switchEstado(estado).Estado;
                    }


                    connection.Close();
                }
                return endereco;
            }
            catch (Exception)
            {
                return endereco = null;
            }
        }

        public bool AtualizarDados(Endereco endereco)
        {
            SqlTransaction sqlTransaction = null;

            try
            {
                Conexao objConexao = new Conexao();
                SqlCommand command = new SqlCommand();

                using (var connection = new SqlConnection(objConexao.getConexao()))
                {
                    command.Connection = connection;

                    connection.Open();

                    sqlTransaction = connection.BeginTransaction();

                    command.Transaction = sqlTransaction;

                    command.CommandText = @"UPDATE Endereco
                                               SET Rua = @Rua,
                                                   Numero = @Numero,
                                                   Complemento = @Complemento,
                                                   Bairro = @Bairro,
                                                   CEP = @CEP,
                                                   Cidade = @Cidade,
                                                   Estado = @Estado
                                             WHERE ID = @ID";

                    command.Parameters.AddWithValue("@Rua", endereco.Rua);
                    command.Parameters.AddWithValue("@Numero", endereco.Numero);
                    command.Parameters.AddWithValue("@Complemento", endereco.Complemento);
                    command.Parameters.AddWithValue("@Bairro", endereco.Bairro);
                    command.Parameters.AddWithValue("@CEP", endereco.CEP);
                    command.Parameters.AddWithValue("@Cidade", endereco.Cidade);
                    command.Parameters.AddWithValue("@Estado", endereco.Estado);
                    command.Parameters.AddWithValue("@ID", endereco.ID);
                    command.ExecuteNonQuery();

                    sqlTransaction.Commit();

                    connection.Close();

                    return true;
                }
            }
            catch (Exception)
            {
                sqlTransaction.Rollback();
                return false;
            }
        }

        private Endereco switchEstado(string estado)
        {
            Endereco endereco = new Endereco();

            switch (estado)
            {
                case "AC":
                    endereco.Estado = EnumEstado.AC;
                    break;

                case "AL":
                    endereco.Estado = EnumEstado.AL;
                    break;

                case "AP":
                    endereco.Estado = EnumEstado.AP;
                    break;

                case "AM":
                    endereco.Estado = EnumEstado.AM;
                    break;

                case "BA":
                    endereco.Estado = EnumEstado.BA;
                    break;

                case "CE":
                    endereco.Estado = EnumEstado.CE;
                    break;

                case "DF":
                    endereco.Estado = EnumEstado.DF;
                    break;

                case "ES":
                    endereco.Estado = EnumEstado.ES;
                    break;

                case "GO":
                    endereco.Estado = EnumEstado.GO;
                    break;

                case "MA":
                    endereco.Estado = EnumEstado.MA;
                    break;

                case "MT":
                    endereco.Estado = EnumEstado.MT;
                    break;

                case "MS":
                    endereco.Estado = EnumEstado.MS;
                    break;

                case "MG":
                    endereco.Estado = EnumEstado.MG;
                    break;

                case "PA":
                    endereco.Estado = EnumEstado.PA;
                    break;

                case "PB":
                    endereco.Estado = EnumEstado.PB;
                    break;

                case "PR":
                    endereco.Estado = EnumEstado.PR;
                    break;

                case "PE":
                    endereco.Estado = EnumEstado.PE;
                    break;

                case "PI":
                    endereco.Estado = EnumEstado.PI;
                    break;

                case "RJ":
                    endereco.Estado = EnumEstado.RJ;
                    break;

                case "RN":
                    endereco.Estado = EnumEstado.RN;
                    break;

                case "RS":
                    endereco.Estado = EnumEstado.RS;
                    break;

                case "RO":
                    endereco.Estado = EnumEstado.RO;
                    break;

                case "RR":
                    endereco.Estado = EnumEstado.RR;
                    break;

                case "SC":
                    endereco.Estado = EnumEstado.SC;
                    break;

                case "SP":
                    endereco.Estado = EnumEstado.SP;
                    break;

                case "SE":
                    endereco.Estado = EnumEstado.SE;
                    break;

                case "TO":
                    endereco.Estado = EnumEstado.TO;
                    break;
            }

            return endereco;
        }
    }
}