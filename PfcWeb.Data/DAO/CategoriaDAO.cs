﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using PfcWeb.Data.Connection;
using PfcWeb.Domain.Entities;

namespace PFC.Data.DAO
{
    public class CategoriaDAO
    {
        public bool Cadastrar(Categoria categoria)
        {
            SqlTransaction sqlTransaction = null;

            try
            {
                Conexao objConexao = new Conexao();
                SqlCommand command = new SqlCommand();

                using (var connection = new SqlConnection(objConexao.getConexao()))
                {
                    command.Connection = connection;

                    connection.Open();

                    sqlTransaction = connection.BeginTransaction();

                    command.Transaction = sqlTransaction;

                    command.CommandText = @"INSERT INTO Categoria (Descricao)
                                                             VALUES (@Descricao)";

                    command.Parameters.AddWithValue("@Descricao", categoria.Descricao);

                    command.ExecuteNonQuery();

                    sqlTransaction.Commit();

                    connection.Close();

                    return true;
                }
            }
            catch (Exception)
            {
                sqlTransaction.Rollback();
                return false;
            }
        }

        public bool Desativar(int id)
        {
            SqlTransaction sqlTransaction = null;

            try
            {
                Conexao objConexao = new Conexao();
                SqlCommand command = new SqlCommand();

                using (var connection = new SqlConnection(objConexao.getConexao()))
                {
                    command.Connection = connection;

                    connection.Open();

                    sqlTransaction = connection.BeginTransaction();

                    command.Transaction = sqlTransaction;

                    command.CommandText = @"UPDATE Categoria
                                               SET Status = 0
                                             WHERE ID = @ID";

                    command.Parameters.AddWithValue("@ID", id);
                    command.ExecuteNonQuery();

                    sqlTransaction.Commit();

                    connection.Close();

                    return true;
                }
            }
            catch (Exception)
            {
                sqlTransaction.Rollback();
                return false;
            }
        }

        public List<Categoria> ListarTodos()
        {
            List<Categoria> lstCategoria = new List<Categoria>();
            try
            {
                Conexao objConexao = new Conexao();
                SqlCommand command = new SqlCommand();

                using (var connection = new SqlConnection(objConexao.getConexao()))
                {
                    command.Connection = connection;

                    connection.Open();

                    command.CommandText = @"SELECT * FROM Categoria
                                                    ORDER BY ID;";

                    SqlDataReader dr = command.ExecuteReader();

                    while (dr.Read())
                    {
                        Categoria categoria = new Categoria();

                        categoria.ID = Convert.ToInt32(dr["ID"]);
                        categoria.Descricao = dr["Descricao"].ToString();

                        lstCategoria.Add(categoria);
                    }

                    connection.Close();
                }
                return lstCategoria;
            }
            catch (Exception)
            {
                throw;
            }
        }

        public Categoria ListarSelecionadoNome(string descricao)
        {
            Categoria categoria = new Categoria();
            try
            {
                Conexao objConexao = new Conexao();
                SqlCommand command = new SqlCommand();

                using (var connection = new SqlConnection(objConexao.getConexao()))
                {
                    command.Connection = connection;

                    connection.Open();

                    command.CommandText = @"SELECT * FROM Categoria
                                                    WHERE Descricao = @Descricao
                                                    ORDER BY ID;";

                    command.Parameters.AddWithValue("@Descricao", descricao);

                    SqlDataReader dr = command.ExecuteReader();

                    dr.Read();

                    categoria.ID = Convert.ToInt32(dr["ID"]);
                    categoria.Descricao = dr["Descricao"].ToString();

                    connection.Close();
                }
                return categoria;
            }
            catch (Exception)
            {
                return categoria = null;
            }
        }

        public Categoria ListarSelecionadoID(int id)
        {
            Categoria categoria = new Categoria();
            try
            {
                Conexao objConexao = new Conexao();
                SqlCommand command = new SqlCommand();

                using (var connection = new SqlConnection(objConexao.getConexao()))
                {
                    command.Connection = connection;

                    connection.Open();

                    command.CommandText = @"SELECT * FROM Categoria
                                                    WHERE ID = @ID;";

                    command.Parameters.AddWithValue("@ID", id);

                    SqlDataReader dr = command.ExecuteReader();

                    dr.Read();

                    categoria.ID = Convert.ToInt32(dr["ID"]);
                    categoria.Descricao = dr["Descricao"].ToString();

                    connection.Close();
                }
                return categoria;
            }
            catch (Exception)
            {
                return categoria = null;
            }
        }

        public bool AtualizarDados(Categoria categoria)
        {
            SqlTransaction sqlTransaction = null;

            try
            {
                Conexao objConexao = new Conexao();
                SqlCommand command = new SqlCommand();

                using (var connection = new SqlConnection(objConexao.getConexao()))
                {
                    command.Connection = connection;

                    connection.Open();

                    sqlTransaction = connection.BeginTransaction();

                    command.Transaction = sqlTransaction;

                    command.CommandText = @"UPDATE Categoria
                                               SET Descricao = @Descricao
                                             WHERE ID = @ID";


                    command.Parameters.AddWithValue("@Descricao", categoria.Descricao);
                    command.Parameters.AddWithValue("@ID", categoria.ID);

                    command.ExecuteNonQuery();

                    sqlTransaction.Commit();

                    connection.Close();

                    return true;
                }
            }
            catch (Exception)
            {
                sqlTransaction.Rollback();
                return false;
            }
        }
    }
}