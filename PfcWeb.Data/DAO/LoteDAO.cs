﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using PfcWeb.Domain.Entities;
using System.Data.SqlClient;
using PfcWeb.Data.Connection;

namespace PfcWeb.Data.DAO
{
    public class LoteDAO
    {
        public bool Cadastrar(Lote lote)
        {
            SqlTransaction sqlTransaction = null;

            try
            {
                Conexao objConexao = new Conexao();
                SqlCommand command = new SqlCommand();

                using (var connection = new SqlConnection(objConexao.getConexao()))
                {
                    command.Connection = connection;

                    connection.Open();

                    sqlTransaction = connection.BeginTransaction();

                    command.Transaction = sqlTransaction;

                    command.CommandText = @"INSERT INTO Lote (QuantidadeItens,
                                                              DataEntrada,
                                                              Categoria,
                                                              Produto,
                                                              DataValidade,
                                                              Barcode,
                                                              Descricao,
                                                              Status)
                                                      VALUES (@QuantidadeItens,
                                                              @DataEntrada,
                                                              @Categoria,
                                                              @Produto,
                                                              @DataValidade,
                                                              @Barcode,
                                                              @Descricao,
                                                              1)";

                    command.Parameters.AddWithValue("@QuantidadeItens", lote.QuantidadeItens);
                    command.Parameters.AddWithValue("@DataEntrada", lote.DataEntrada);
                    command.Parameters.AddWithValue("@Categoria", lote.Categoria.ID);
                    command.Parameters.AddWithValue("@Produto", lote.Produto.ID);
                    command.Parameters.AddWithValue("@DataValidade", lote.DataValidade);
                    command.Parameters.AddWithValue("@Barcode", lote.Barcode);
                    command.Parameters.AddWithValue("@Descricao", lote.Descricao);

                    command.ExecuteNonQuery();

                    sqlTransaction.Commit();

                    connection.Close();

                    return true;
                }
            }
            catch (Exception)
            {
                sqlTransaction.Rollback();
                return false;
            }
        }

        public List<Lote> ListarVencidos()
        {
            List<Lote> lstLote = new List<Lote>();
            try
            {
                Conexao objConexao = new Conexao();
                SqlCommand command = new SqlCommand();

                using (var connection = new SqlConnection(objConexao.getConexao()))
                {
                    command.Connection = connection;

                    connection.Open();

                    command.CommandText = @"SELECT *
                                              FROM Lote
                                             WHERE DataValidade <= @DataValidade
                                               AND status = 1
                                          ORDER BY DataValidade";


                    command.Parameters.AddWithValue("@DataValidade", DateTime.Now);

                    SqlDataReader dr = command.ExecuteReader();

                    while (dr.Read())
                    {
                        Lote lote = new Lote();

                        lote.ID = Convert.ToInt32(dr["ID"]);
                        lote.QuantidadeItens = Convert.ToInt32(dr["QuantidadeItens"]);
                        lote.DataEntrada = Convert.ToDateTime(dr["DataEntrada"]);
                        lote.Categoria = new Categoria();
                        lote.Categoria.ID = Convert.ToInt32(dr["Categoria"]);
                        lote.Produto = new Produto();
                        lote.Produto.ID = Convert.ToInt32(dr["Produto"]);
                        lote.DataValidade = Convert.ToDateTime(dr["DataValidade"]);
                        lote.Barcode = dr["Barcode"].ToString();
                        lote.Descricao = dr["Descricao"].ToString();
                        lote.Status = Convert.ToBoolean(dr["Status"]);

                        lstLote.Add(lote);
                    }

                    connection.Close();
                }
                return lstLote;
            }
            catch (Exception)
            {
                throw;
            }
        }

        public List<Lote> ListarTodos()
        {
            List<Lote> lstLote = new List<Lote>();
            try
            {
                Conexao objConexao = new Conexao();
                SqlCommand command = new SqlCommand();

                using (var connection = new SqlConnection(objConexao.getConexao()))
                {
                    command.Connection = connection;

                    connection.Open();

                    command.CommandText = @"SELECT *
                                              FROM Lote
                                          ORDER BY DataValidade";

                    SqlDataReader dr = command.ExecuteReader();

                    while (dr.Read())
                    {
                        Lote lote = new Lote();

                        lote.ID = Convert.ToInt32(dr["ID"]);
                        lote.QuantidadeItens = Convert.ToInt32(dr["QuantidadeItens"]);
                        lote.DataEntrada = Convert.ToDateTime(dr["DataEntrada"]);
                        lote.Categoria = new Categoria();
                        lote.Categoria.ID = Convert.ToInt32(dr["Categoria"]);
                        lote.Produto = new Produto();
                        lote.Produto.ID = Convert.ToInt32(dr["Produto"]);
                        lote.DataValidade = Convert.ToDateTime(dr["DataValidade"]);
                        lote.Barcode = dr["Barcode"].ToString();
                        lote.Descricao = dr["Descricao"].ToString();
                        lote.Status = Convert.ToBoolean(dr["Status"]);

                        lstLote.Add(lote);
                    }

                    connection.Close();
                }
                return lstLote;
            }
            catch (Exception)
            {
                throw;
            }
        }

        public Lote ListarSelecionadoID(int ID)
        {
            Lote lote = new Lote();
            try
            {
                Conexao objConexao = new Conexao();
                SqlCommand command = new SqlCommand();

                using (var connection = new SqlConnection(objConexao.getConexao()))
                {
                    command.Connection = connection;

                    connection.Open();

                    command.CommandText = @"SELECT *
                                              FROM Lote
                                             WHERE ID = @ID";

                    command.Parameters.AddWithValue("@ID", ID);

                    SqlDataReader dr = command.ExecuteReader();

                    dr.Read();

                    lote.ID = Convert.ToInt32(dr["ID"]);
                    lote.QuantidadeItens = Convert.ToInt32(dr["QuantidadeItens"]);
                    lote.DataEntrada = Convert.ToDateTime(dr["DataEntrada"]);
                    lote.Categoria = new Categoria();
                    lote.Categoria.ID = Convert.ToInt32(dr["Categoria"]);
                    lote.Produto = new Produto();
                    lote.Produto.ID = Convert.ToInt32(dr["Produto"]);
                    lote.DataValidade = Convert.ToDateTime(dr["DataValidade"]);
                    lote.Barcode = dr["Barcode"].ToString();
                    lote.Descricao = dr["Descricao"].ToString();

                    connection.Close();
                }
                return lote;
            }
            catch (Exception)
            {
                return lote = null;
            }
        }

        public Lote ListarSelecionadoBarcode(string barcode)
        {
            Lote lote = new Lote();
            try
            {
                Conexao objConexao = new Conexao();
                SqlCommand command = new SqlCommand();

                using (var connection = new SqlConnection(objConexao.getConexao()))
                {
                    command.Connection = connection;

                    connection.Open();

                    command.CommandText = @"SELECT *
                                              FROM Lote
                                             WHERE Barcode = @Barcode";

                    command.Parameters.AddWithValue("@Barcode", barcode);

                    SqlDataReader dr = command.ExecuteReader();

                    dr.Read();

                    lote.ID = Convert.ToInt32(dr["ID"]);
                    lote.QuantidadeItens = Convert.ToInt32(dr["QuantidadeItens"]);
                    lote.DataEntrada = Convert.ToDateTime(dr["DataEntrada"]);
                    lote.Categoria = new Categoria();
                    lote.Categoria.ID = Convert.ToInt32(dr["Categoria"]);
                    lote.Produto = new Produto();
                    lote.Produto.ID = Convert.ToInt32(dr["Produto"]);
                    lote.DataValidade = Convert.ToDateTime(dr["DataValidade"]);
                    lote.Barcode = dr["Barcode"].ToString();
                    lote.Descricao = dr["Descricao"].ToString();
                    lote.Status = Convert.ToBoolean(dr["Status"]);

                    connection.Close();
                }
                return lote;
            }
            catch (Exception)
            {
                return lote = null;
            }
        }

        public List<Lote> ListarDesativadosPorCategoria(Categoria categoria)
        {
            List<Lote> lstLote = new List<Lote>();
            try
            {
                Conexao objConexao = new Conexao();
                SqlCommand command = new SqlCommand();

                using (var connection = new SqlConnection(objConexao.getConexao()))
                {
                    command.Connection = connection;

                    connection.Open();

                    command.CommandText = @"SELECT *
                                              FROM Lote
                                             WHERE Status = 0
                                               AND Categoria = @Categoria
                                          ORDER BY DataValidade";
                    
                    command.Parameters.AddWithValue("@Categoria", categoria.ID);

                    SqlDataReader dr = command.ExecuteReader();

                    while (dr.Read())
                    {
                        Lote lote = new Lote();

                        lote.ID = Convert.ToInt32(dr["ID"]);
                        lote.QuantidadeItens = Convert.ToInt32(dr["QuantidadeItens"]);
                        lote.DataEntrada = Convert.ToDateTime(dr["DataEntrada"]);
                        lote.Categoria = new Categoria();
                        lote.Categoria.ID = Convert.ToInt32(dr["Categoria"]);
                        lote.Produto = new Produto();
                        lote.Produto.ID = Convert.ToInt32(dr["Produto"]);
                        lote.DataValidade = Convert.ToDateTime(dr["DataValidade"]);
                        lote.Barcode = dr["Barcode"].ToString();
                        lote.Descricao = dr["Descricao"].ToString();
                        lote.Status = Convert.ToBoolean(dr["Status"]);

                        lstLote.Add(lote);
                    }

                    connection.Close();
                }
                return lstLote;
            }
            catch (Exception)
            {
                throw;
            }
        }

        public bool AtualizarDados(Lote lote)
        {
            SqlTransaction sqlTransaction = null;

            try
            {
                Conexao objConexao = new Conexao();
                SqlCommand command = new SqlCommand();

                using (var connection = new SqlConnection(objConexao.getConexao()))
                {
                    command.Connection = connection;

                    connection.Open();

                    sqlTransaction = connection.BeginTransaction();

                    command.Transaction = sqlTransaction;

                    command.CommandText = @"UPDATE Lote
                                               SET QuantidadeItens = @QuantidadeItens,
                                                   DataEntrada = @DataEntrada,
                                                   Categoria = @Categoria,
                                                   Produto = @Produto,
                                                   DataValidade = @DataValidade,
                                                   Barcode = @Barcode,
                                                   Descricao = @Descricao
                                             WHERE ID = @ID";

                    command.Parameters.AddWithValue("@QuantidadeItens", lote.QuantidadeItens);
                    command.Parameters.AddWithValue("@DataEntrada", lote.DataEntrada);
                    command.Parameters.AddWithValue("@Categoria", lote.Categoria.ID);
                    command.Parameters.AddWithValue("@Produto", lote.Produto.ID);
                    command.Parameters.AddWithValue("@DataValidade", lote.DataValidade);
                    command.Parameters.AddWithValue("@Barcode", lote.Barcode);
                    command.Parameters.AddWithValue("@Descricao", lote.Descricao);
                    command.Parameters.AddWithValue("@ID", lote.ID);
                    command.ExecuteNonQuery();

                    sqlTransaction.Commit();

                    connection.Close();

                    return true;
                }
            }
            catch (Exception)
            {
                sqlTransaction.Rollback();
                return false;
            }
        }

        public bool AtualizarQuantidade(int id, long quantidadeNova)
        {
            SqlTransaction sqlTransaction = null;

            try
            {
                Conexao objConexao = new Conexao();
                SqlCommand command = new SqlCommand();

                using (var connection = new SqlConnection(objConexao.getConexao()))
                {
                    command.Connection = connection;

                    connection.Open();

                    sqlTransaction = connection.BeginTransaction();

                    command.Transaction = sqlTransaction;

                    command.CommandText = @"UPDATE Lote
                                               SET QuantidadeItens = @QuantidadeItens
                                             WHERE ID = @ID";

                    command.Parameters.AddWithValue("@QuantidadeItens", quantidadeNova);
                    command.Parameters.AddWithValue("@ID", id);
                    command.ExecuteNonQuery();

                    sqlTransaction.Commit();

                    connection.Close();

                    return true;
                }
            }
            catch (Exception)
            {
                sqlTransaction.Rollback();
                return false;
            }
        }

        public bool DesativarLote(int id)
        {
            SqlTransaction sqlTransaction = null;

            try
            {
                Conexao objConexao = new Conexao();
                SqlCommand command = new SqlCommand();

                using (var connection = new SqlConnection(objConexao.getConexao()))
                {
                    command.Connection = connection;

                    connection.Open();

                    sqlTransaction = connection.BeginTransaction();

                    command.Transaction = sqlTransaction;

                    command.CommandText = @"UPDATE Lote
                                               SET Status = 0
                                             WHERE ID = @ID";

                    command.Parameters.AddWithValue("@ID", id);
                    command.ExecuteNonQuery();

                    sqlTransaction.Commit();

                    connection.Close();

                    return true;
                }
            }
            catch (Exception)
            {
                sqlTransaction.Rollback();
                return false;
            }
        }
    }
}