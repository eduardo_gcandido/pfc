﻿using PfcWeb.Data.Connection;
using PfcWeb.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PfcWeb.Data.DAO
{
    public class ItemVendaDAO
    {
        public bool Cadastrar(ItemVenda itemVenda)
        {
            SqlTransaction sqlTransaction = null;

            try
            {
                Conexao objConexao = new Conexao();
                SqlCommand command = new SqlCommand();

                using (var connection = new SqlConnection(objConexao.getConexao()))
                {
                    command.Connection = connection;

                    connection.Open();

                    sqlTransaction = connection.BeginTransaction();

                    command.Transaction = sqlTransaction;

                    command.CommandText = @"INSERT INTO ItemVenda (Venda,
                                                                   Lote,
                                                                   Quantidade)
                                                          VALUES (@Venda,
                                                                  @Lote,
                                                                  @Quantidade)";

                    command.Parameters.AddWithValue("@Venda", itemVenda.Venda.ID);
                    command.Parameters.AddWithValue("@Lote", itemVenda.Lote.ID);
                    command.Parameters.AddWithValue("@Quantidade", itemVenda.Quantidade);

                    command.ExecuteNonQuery();

                    sqlTransaction.Commit();

                    connection.Close();

                    return true;
                }
            }
            catch (Exception)
            {
                sqlTransaction.Rollback();
                return false;
            }
        }

        public void DeletarItemVendaNaoFinalizado(ItemVenda itemVenda)
        {
            SqlTransaction sqlTransaction = null;

            try
            {
                Conexao objConexao = new Conexao();
                SqlCommand command = new SqlCommand();

                using (var connection = new SqlConnection(objConexao.getConexao()))
                {
                    command.Connection = connection;

                    connection.Open();

                    sqlTransaction = connection.BeginTransaction();

                    command.Transaction = sqlTransaction;

                    command.CommandText = @"DELETE FROM ItemVenda
                                                  WHERE ID = @ID";

                    command.Parameters.AddWithValue("@ID", itemVenda.ID);

                    command.ExecuteNonQuery();

                    sqlTransaction.Commit();

                    connection.Close();
                }
            }
            catch (Exception)
            {
                sqlTransaction.Rollback();
                throw;
            }
        }

        public List<ItemVenda> ListarPorIDVenda(Venda venda)
        {
            List<ItemVenda> listItemVenda = new List<ItemVenda>();
            try
            {
                Conexao objConexao = new Conexao();
                SqlCommand command = new SqlCommand();

                using (var connection = new SqlConnection(objConexao.getConexao()))
                {
                    command.Connection = connection;

                    connection.Open();

                    command.CommandText = @"SELECT * FROM ItemVenda
                                                    WHERE Venda = @Venda
                                                    ORDER BY Venda;";


                    command.Parameters.AddWithValue("@Venda", venda.ID);

                    SqlDataReader dr = command.ExecuteReader();

                    while (dr.Read())
                    {
                        ItemVenda itemVenda = new ItemVenda();

                        itemVenda.ID = Convert.ToInt32(dr["ID"]);
                        itemVenda.Lote = new Lote();
                        itemVenda.Lote.ID = Convert.ToInt32(dr["Lote"]);
                        itemVenda.Quantidade = Convert.ToInt32(dr["Quantidade"]);
                        itemVenda.Venda = new Venda();
                        itemVenda.Venda.ID = Convert.ToInt32(dr["Venda"]);

                        listItemVenda.Add(itemVenda);
                    }

                    connection.Close();
                }
                return listItemVenda;
            }
            catch (Exception)
            {
                throw;
            }
        }
    }
}