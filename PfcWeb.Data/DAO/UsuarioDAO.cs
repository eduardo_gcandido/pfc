﻿using PfcWeb.Domain.Entities;
using System;
using Microsoft.SqlServer;
using System.Data.SqlClient;
using System.Collections.Generic;
using PfcWeb.Domain.Enum;
using PfcWeb.Data.Connection;
using System.Data;

namespace PfcWeb.Data.DAO
{
    public class UsuarioDAO
    {
        public bool Cadastrar(Usuario usuario)
        {
            SqlTransaction sqlTransaction = null;

            try
            {
                Conexao objConexao = new Conexao();
                SqlCommand command = new SqlCommand();

                using (var connection = new SqlConnection(objConexao.getConexao()))
                {
                    command.Connection = connection;

                    connection.Open();

                    sqlTransaction = connection.BeginTransaction();

                    command.Transaction = sqlTransaction;

                    command.CommandText = @"INSERT INTO Usuario (Login,
                                                                 Senha,
                                                                 NivelAcesso,
                                                                 Status)
                                                         values (@Login,
                                                                 @Senha,
                                                                 @NivelAcesso,
                                                                 1)";

                    command.Parameters.AddWithValue("@Login", usuario.Login);
                    command.Parameters.AddWithValue("@Senha", usuario.Senha);
                    command.Parameters.AddWithValue("@NivelAcesso", usuario.NivelAcesso.ToString());

                    command.ExecuteNonQuery();

                    sqlTransaction.Commit();

                    connection.Close();

                    return true;
                }
            }
            catch (Exception)
            {
                sqlTransaction.Rollback();
                return false;
            }
        }

        public Usuario EfetuarLogin(string login)
        {
            Usuario usuario = new Usuario();
            try
            {
                Conexao objConexao = new Conexao();
                SqlCommand command = new SqlCommand();

                using (var connection = new SqlConnection(objConexao.getConexao()))
                {
                    command.Connection = connection;

                    connection.Open();

                    command.CommandText = @"SELECT *
                                              FROM Usuario
                                             WHERE Login = @Login";

                    command.Parameters.AddWithValue("@Login", login);

                    SqlDataReader dr = command.ExecuteReader();

                    dr.Read();

                    usuario.ID = Convert.ToInt32(dr["ID"]);
                    usuario.Login = dr["Login"].ToString();
                    usuario.Senha = dr["Senha"].ToString();
                    var nivel = dr["NivelAcesso"].ToString();
                    usuario.Status = Convert.ToBoolean(dr["Status"]);

                    switch (nivel)
                    {
                        case "ADMINISTRADOR":
                            usuario.NivelAcesso = EnumNivelAcesso.GERENTE;
                            break;

                        case "CAIXA":
                            usuario.NivelAcesso = EnumNivelAcesso.CAIXA;
                            break;

                        case "ESTOQUE":
                            usuario.NivelAcesso = EnumNivelAcesso.ESTOQUE;
                            break;
                    }

                    connection.Close();
                }
                return usuario;
            }
            catch (Exception)
            {
                return usuario = null;
            }
        }

        public List<Usuario> ListarTodos()
        {
            List<Usuario> lstUsuario = new List<Usuario>();
            try
            {
                Conexao objConexao = new Conexao();
                SqlCommand command = new SqlCommand();

                using (var connection = new SqlConnection(objConexao.getConexao()))
                {
                    command.Connection = connection;

                    connection.Open();

                    command.CommandText = @"SELECT * FROM Usuario
                                                    ORDER BY ID;";

                    SqlDataReader dr = command.ExecuteReader();

                    while (dr.Read())
                    {
                        Usuario usuario = new Usuario()
                        {
                            ID = Convert.ToInt32(dr["ID"]),
                            Login = dr["Login"].ToString(),
                            Senha = dr["Senha"].ToString(),
                            Status = Convert.ToBoolean(dr["Status"])
                        };
                        lstUsuario.Add(usuario);
                    }

                    connection.Close();
                }
                return lstUsuario;
            }
            catch (Exception)
            {
                throw;
            }
        }

        public Usuario ListarSelecionado(int id)
        {
            Usuario usuario = new Usuario();
            try
            {
                Conexao objConexao = new Conexao();
                SqlCommand command = new SqlCommand();

                using (var connection = new SqlConnection(objConexao.getConexao()))
                {
                    command.Connection = connection;

                    connection.Open();

                    command.CommandText = @"SELECT * FROM Usuario
                                                    WHERE ID = @ID
                                                    ORDER BY ID;";

                    command.Parameters.AddWithValue("@ID", id);

                    SqlDataReader dr = command.ExecuteReader();

                    dr.Read();

                    usuario.ID = Convert.ToInt32(dr["ID"]);
                    usuario.Login = dr["Login"].ToString();
                    usuario.Senha = dr["Senha"].ToString();
                    string nivel = dr["NivelAcesso"].ToString();
                    usuario.Status = Convert.ToBoolean(dr["Status"]);

                    switch (nivel)
                    {
                        case "ADMINISTRADOR":
                            usuario.NivelAcesso = EnumNivelAcesso.GERENTE;
                            break;

                        case "CAIXA":
                            usuario.NivelAcesso = EnumNivelAcesso.CAIXA;
                            break;

                        case "ESTOQUE":
                            usuario.NivelAcesso = EnumNivelAcesso.ESTOQUE;
                            break;
                    }

                    connection.Close();
                }
                return usuario;
            }
            catch (Exception)
            {
                return usuario = null;
            }
        }

        public bool Ativar(int id)
        {
            SqlTransaction sqlTransaction = null;

            try
            {
                Conexao objConexao = new Conexao();
                SqlCommand command = new SqlCommand();

                using (var connection = new SqlConnection(objConexao.getConexao()))
                {
                    command.Connection = connection;

                    connection.Open();

                    sqlTransaction = connection.BeginTransaction();

                    command.Transaction = sqlTransaction;

                    command.CommandText = @"UPDATE Usuario
                                               SET Status = 1
                                             WHERE ID = @ID";

                    command.Parameters.AddWithValue("@ID", id);
                    command.ExecuteNonQuery();

                    sqlTransaction.Commit();

                    connection.Close();

                    return true;
                }
            }
            catch (Exception)
            {
                sqlTransaction.Rollback();
                return false;
            }
        }

        public bool AtualizarDados(Usuario usuario)
        {
            SqlTransaction sqlTransaction = null;

            try
            {
                Conexao objConexao = new Conexao();
                SqlCommand command = new SqlCommand();

                using (var connection = new SqlConnection(objConexao.getConexao()))
                {
                    command.Connection = connection;

                    connection.Open();

                    sqlTransaction = connection.BeginTransaction();

                    command.Transaction = sqlTransaction;

                    command.CommandText = @"UPDATE Usuario
                                               SET Login = @Login,
                                                   Senha = @senha,
                                                   NivelAcesso = @NivelAcesso
                                             WHERE ID = @ID";

                    command.Parameters.AddWithValue("@Login", usuario.Login);
                    command.Parameters.AddWithValue("@Senha", usuario.Senha);
                    command.Parameters.AddWithValue("@NivelAcesso", usuario.NivelAcesso.ToString());
                    command.Parameters.AddWithValue("@ID", usuario.ID);
                    command.ExecuteNonQuery();

                    sqlTransaction.Commit();

                    connection.Close();

                    return true;
                }
            }
            catch (Exception)
            {
                sqlTransaction.Rollback();
                return false;
            }
        }
        
        public bool Desativar(int id)
        {
            SqlTransaction sqlTransaction = null;

            try
            {
                Conexao objConexao = new Conexao();
                SqlCommand command = new SqlCommand();

                using (var connection = new SqlConnection(objConexao.getConexao()))
                {
                    command.Connection = connection;

                    connection.Open();

                    sqlTransaction = connection.BeginTransaction();

                    command.Transaction = sqlTransaction;

                    command.CommandText = @"UPDATE Usuario
                                               SET Status = 0
                                             WHERE ID = @ID";
                    
                    command.Parameters.AddWithValue("@ID", id);
                    command.ExecuteNonQuery();

                    sqlTransaction.Commit();

                    connection.Close();

                    return true;
                }
            }
            catch (Exception)
            {
                sqlTransaction.Rollback();
                return false;
            }
        }
    }
}