﻿using PfcWeb.Data.Connection;
using PfcWeb.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PfcWeb.Data.DAO
{
    public class ProdutoDAO
    {
        public bool Cadastrar(Produto produto)
        {
            SqlTransaction sqlTransaction = null;

            try
            {
                Conexao objConexao = new Conexao();
                SqlCommand command = new SqlCommand();

                using (var connection = new SqlConnection(objConexao.getConexao()))
                {
                    command.Connection = connection;

                    connection.Open();

                    sqlTransaction = connection.BeginTransaction();

                    command.Transaction = sqlTransaction;

                    command.CommandText = @"INSERT INTO Produto (Nome,
                                                                 ValorUnitario)
                                                        VALUES (@Nome,
                                                                @ValorUnitario)";

                    command.Parameters.AddWithValue("@Nome", produto.Nome);
                    command.Parameters.AddWithValue("@ValorUnitario", produto.ValorUnitario);

                    command.ExecuteNonQuery();

                    sqlTransaction.Commit();

                    connection.Close();

                    return true;
                }
            }
            catch (Exception)
            {
                sqlTransaction.Rollback();
                return false;
            }
        }

        public List<Produto> ListarTodos()
        {
            List<Produto> lstProduto = new List<Produto>();
            try
            {
                Conexao objConexao = new Conexao();
                SqlCommand command = new SqlCommand();

                using (var connection = new SqlConnection(objConexao.getConexao()))
                {
                    command.Connection = connection;

                    connection.Open();

                    command.CommandText = @"SELECT * FROM Produto
                                                    ORDER BY ID;";

                    SqlDataReader dr = command.ExecuteReader();

                    while (dr.Read())
                    {
                        Produto produto = new Produto();

                        produto.ID = Convert.ToInt32(dr["ID"]);
                        produto.Nome = dr["Nome"].ToString();
                        produto.ValorUnitario = Convert.ToDouble(dr["ValorUnitario"]);

                        lstProduto.Add(produto);
                    }

                    connection.Close();
                }
                return lstProduto;
            }
            catch (Exception)
            {
                throw;
            }
        }

        public Produto ListarSelecionadoID(int ID)
        {
            Produto produto = new Produto();
            try
            {
                Conexao objConexao = new Conexao();
                SqlCommand command = new SqlCommand();

                using (var connection = new SqlConnection(objConexao.getConexao()))
                {
                    command.Connection = connection;

                    connection.Open();

                    command.CommandText = @"SELECT * FROM Produto
                                                    WHERE ID = @ID;";

                    command.Parameters.AddWithValue("@ID", ID);

                    SqlDataReader dr = command.ExecuteReader();

                    dr.Read();

                    produto.ID = Convert.ToInt32(dr["ID"]);
                    produto.Nome = dr["Nome"].ToString();
                    produto.ValorUnitario = Convert.ToDouble(dr["ValorUnitario"]);

                    connection.Close();
                }
                return produto;
            }
            catch (Exception)
            {
                return produto = null;
            }
        }

        public bool AtualizarDados(Produto produto)
        {
            SqlTransaction sqlTransaction = null;

            try
            {
                Conexao objConexao = new Conexao();
                SqlCommand command = new SqlCommand();

                using (var connection = new SqlConnection(objConexao.getConexao()))
                {
                    command.Connection = connection;

                    connection.Open();

                    sqlTransaction = connection.BeginTransaction();

                    command.Transaction = sqlTransaction;

                    command.CommandText = @"UPDATE Produto
                                               SET Nome = @Nome,
                                                   ValorUnitario = @ValorUnitario
                                             WHERE ID = @ID";

                    command.Parameters.AddWithValue("@Nome", produto.Nome);
                    command.Parameters.AddWithValue("@ValorUnitario", produto.ValorUnitario);
                    command.Parameters.AddWithValue("@ID", produto.ID);
                    command.ExecuteNonQuery();

                    sqlTransaction.Commit();

                    connection.Close();

                    return true;
                }
            }
            catch (Exception)
            {
                sqlTransaction.Rollback();
                return false;
            }
        }
    }
}