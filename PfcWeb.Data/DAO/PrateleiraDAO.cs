﻿using PfcWeb.Data.Connection;
using PfcWeb.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PFC.Data.DAO
{
    public class PrateleiraDAO
    {
        public bool Cadastrar(Prateleira prateleira)
        {
            SqlTransaction sqlTransaction = null;

            try
            {
                Conexao objConexao = new Conexao();
                SqlCommand command = new SqlCommand();

                using (var connection = new SqlConnection(objConexao.getConexao()))
                {
                    command.Connection = connection;

                    connection.Open();

                    sqlTransaction = connection.BeginTransaction();

                    command.Transaction = sqlTransaction;

                    command.CommandText = @"INSERT INTO Prateleira (Descricao,
                                                                    Quantidade,
                                                                    Lote)           
                                                           VALUES (@Descricao,
                                                                   @Quantidade,
                                                                   @Lote)";

                    command.Parameters.AddWithValue("@Descricao", prateleira.Descricao);
                    command.Parameters.AddWithValue("@Quantidade", prateleira.Quantidade);
                    command.Parameters.AddWithValue("@Lote", prateleira.Lote.ID);

                    command.ExecuteNonQuery();

                    sqlTransaction.Commit();

                    connection.Close();

                    return true;
                }
            }
            catch (Exception)
            {
                sqlTransaction.Rollback();
                return false;
            }
        }

        public List<Prateleira> ListarTodos()
        {
            List<Prateleira> lstPrateleira = new List<Prateleira>();
            try
            {
                Conexao objConexao = new Conexao();
                SqlCommand command = new SqlCommand();

                using (var connection = new SqlConnection(objConexao.getConexao()))
                {
                    command.Connection = connection;

                    connection.Open();

                    command.CommandText = @"SELECT * FROM Prateleira
                                                    ORDER BY ID;";

                    SqlDataReader dr = command.ExecuteReader();

                    while (dr.Read())
                    {
                        Prateleira prateleira = new Prateleira();

                        prateleira.ID = Convert.ToInt32(dr["ID"]);
                        prateleira.Descricao = dr["Descricao"].ToString();
                        prateleira.Quantidade = Convert.ToInt32(dr["Quantidade"]);
                        prateleira.Lote = new Lote();
                        prateleira.Lote.ID = Convert.ToInt32(dr["Lote"]);

                        lstPrateleira.Add(prateleira);
                    }

                    connection.Close();
                }
                return lstPrateleira;
            }
            catch (Exception)
            {
                throw;
            }
        }

        public Prateleira ListarSelecionadoID(int ID)
        {
            Prateleira prateleira = new Prateleira();
            try
            {
                Conexao objConexao = new Conexao();
                SqlCommand command = new SqlCommand();

                using (var connection = new SqlConnection(objConexao.getConexao()))
                {
                    command.Connection = connection;

                    connection.Open();

                    command.CommandText = @"SELECT * FROM Prateleira
                                                    WHERE ID = @ID;";

                    command.Parameters.AddWithValue("@ID", ID);

                    SqlDataReader dr = command.ExecuteReader();

                    dr.Read();

                    prateleira.ID = Convert.ToInt32(dr["ID"]);
                    prateleira.Descricao = dr["Descricao"].ToString();
                    prateleira.Quantidade = Convert.ToInt32(dr["Quantidade"]);
                    prateleira.Lote = new Lote();
                    prateleira.Lote.ID = Convert.ToInt32(dr["Lote"]);

                    connection.Close();
                }
                return prateleira;
            }
            catch (Exception)
            {
                return prateleira = null;
            }
        }

        public List<Prateleira> ListarQuaseAcabando()
        {
            List<Prateleira> lstPrateleira = new List<Prateleira>();
            try
            {
                Conexao objConexao = new Conexao();
                SqlCommand command = new SqlCommand();

                using (var connection = new SqlConnection(objConexao.getConexao()))
                {
                    command.Connection = connection;

                    connection.Open();

                    command.CommandText = @"SELECT * FROM Prateleira
                                                    WHERE Quantidade < 20
                                                    ORDER BY ID;";

                    SqlDataReader dr = command.ExecuteReader();

                    while (dr.Read())
                    {
                        Prateleira prateleira = new Prateleira();

                        prateleira.ID = Convert.ToInt32(dr["ID"]);
                        prateleira.Descricao = dr["Descricao"].ToString();
                        prateleira.Quantidade = Convert.ToInt32(dr["Quantidade"]);
                        prateleira.Lote = new Lote();
                        prateleira.Lote.ID = Convert.ToInt32(dr["Lote"]);

                        lstPrateleira.Add(prateleira);
                    }

                    connection.Close();
                }
                return lstPrateleira;
            }
            catch (Exception)
            {
                throw;
            }
        }

        public bool AtualizarQuantidade(int id, int quantidadeAtual)
        {
            SqlTransaction sqlTransaction = null;

            try
            {
                Conexao objConexao = new Conexao();
                SqlCommand command = new SqlCommand();

                using (var connection = new SqlConnection(objConexao.getConexao()))
                {
                    command.Connection = connection;

                    connection.Open();

                    sqlTransaction = connection.BeginTransaction();

                    command.Transaction = sqlTransaction;

                    command.CommandText = @"UPDATE Prateleira
                                               SET Quantidade = @Quantidade          
                                             WHERE ID = @ID";
                    
                    command.Parameters.AddWithValue("@Quantidade", quantidadeAtual);
                    command.Parameters.AddWithValue("@ID", id);
                    command.ExecuteNonQuery();

                    sqlTransaction.Commit();

                    connection.Close();

                    return true;
                }
            }
            catch (Exception)
            {
                sqlTransaction.Rollback();
                return false;
            }
        }

        public Prateleira ListarSelecionadoIDLote(int ID)
        {
            Prateleira prateleira = new Prateleira();
            try
            {
                Conexao objConexao = new Conexao();
                SqlCommand command = new SqlCommand();

                using (var connection = new SqlConnection(objConexao.getConexao()))
                {
                    command.Connection = connection;

                    connection.Open();

                    command.CommandText = @"SELECT * FROM Prateleira
                                                    WHERE Lote = @ID;";

                    command.Parameters.AddWithValue("@ID", ID);

                    SqlDataReader dr = command.ExecuteReader();

                    dr.Read();

                    prateleira.ID = Convert.ToInt32(dr["ID"]);
                    prateleira.Descricao = dr["Descricao"].ToString();
                    prateleira.Quantidade = Convert.ToInt32(dr["Quantidade"]);
                    prateleira.Lote = new Lote();
                    prateleira.Lote.ID = Convert.ToInt32(dr["Lote"]);

                    connection.Close();
                }
                return prateleira;
            }
            catch (Exception)
            {
                return prateleira = null;
            }
        }

        public bool AtualizarDados(Prateleira prateleira)
        {
            SqlTransaction sqlTransaction = null;

            try
            {
                Conexao objConexao = new Conexao();
                SqlCommand command = new SqlCommand();

                using (var connection = new SqlConnection(objConexao.getConexao()))
                {
                    command.Connection = connection;

                    connection.Open();

                    sqlTransaction = connection.BeginTransaction();

                    command.Transaction = sqlTransaction;

                    command.CommandText = @"UPDATE Prateleira
                                               SET Descricao = @Descricao,
                                                   Quantidade = @Quantidade,
                                                   Lote = @Lote            
                                             WHERE ID = @ID";

                    command.Parameters.AddWithValue("@Descricao", prateleira.Descricao);
                    command.Parameters.AddWithValue("@Quantidade", prateleira.Quantidade);
                    command.Parameters.AddWithValue("@Lote", prateleira.Lote.ID);
                    command.Parameters.AddWithValue("@ID", prateleira.ID);
                    command.ExecuteNonQuery();

                    sqlTransaction.Commit();

                    connection.Close();

                    return true;
                }
            }
            catch (Exception)
            {
                sqlTransaction.Rollback();
                return false;
            }
        }
    }
}
