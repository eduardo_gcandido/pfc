﻿using PfcWeb.Data.Connection;
using PfcWeb.Domain.Entities;
using PfcWeb.Entities.Enum;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PfcWeb.Data.DAO
{
    public class FuncionarioDAO
    {
        public bool Cadastrar(Funcionario funcionario)
        {
            SqlTransaction sqlTransaction = null;

            try
            {
                Conexao objConexao = new Conexao();
                SqlCommand command = new SqlCommand();

                using (var connection = new SqlConnection(objConexao.getConexao()))
                {
                    command.Connection = connection;

                    connection.Open();

                    sqlTransaction = connection.BeginTransaction();

                    command.Transaction = sqlTransaction;

                    command.CommandText = @"INSERT INTO Funcionario (Nome,
                                                                     CPF,
                                                                     RG,
                                                                     Sexo,
                                                                     Email,
                                                                     Telefone,
                                                                     Cargo,
                                                                     DataAdmissao,
                                                                     Endereco,
                                                                     Departamento)
                                                            VALUES (@Nome,
                                                                    @CPF,
                                                                    @RG,
                                                                    @Sexo,
                                                                    @Email,
                                                                    @Telefone,
                                                                    @Cargo,
                                                                    @DataAdmissao,
                                                                    @Endereco,
                                                                    @Departamento)";

                    command.Parameters.AddWithValue("@Nome", funcionario.Nome);
                    command.Parameters.AddWithValue("@CPF", funcionario.CPF);
                    command.Parameters.AddWithValue("@RG", funcionario.RG);
                    command.Parameters.AddWithValue("@Sexo", funcionario.Sexo);
                    command.Parameters.AddWithValue("@Email", funcionario.Email);
                    command.Parameters.AddWithValue("@Telefone", funcionario.Telefone);
                    command.Parameters.AddWithValue("@Cargo", funcionario.Cargo);
                    command.Parameters.AddWithValue("@DataAdmissao", funcionario.DataAdmissao);
                    command.Parameters.AddWithValue("@Endereco", funcionario.Endereco.ID);
                    command.Parameters.AddWithValue("@Departamento", funcionario.Departamento.ID);

                    command.ExecuteNonQuery();

                    sqlTransaction.Commit();

                    connection.Close();

                    return true;
                }
            }
            catch (Exception)
            {
                sqlTransaction.Rollback();
                return false;
            }
        }

        public List<Funcionario> ListarfuncionarioEstoque()
        {
            List<Funcionario> lstFuncionario = new List<Funcionario>();
            try
            {
                Conexao objConexao = new Conexao();
                SqlCommand command = new SqlCommand();

                using (var connection = new SqlConnection(objConexao.getConexao()))
                {
                    command.Connection = connection;

                    connection.Open();

                    command.CommandText = @"SELECT * FROM Funcionario
                                                    WHERE Departamento = 1
                                                      AND DataDemissao IS null";

                    SqlDataReader dr = command.ExecuteReader();

                    while (dr.Read())
                    {
                        Funcionario funcionario = new Funcionario();

                        funcionario.Nome = dr["Nome"].ToString();
                        funcionario.CPF = dr["CPF"].ToString();
                        funcionario.RG = dr["RG"].ToString();
                        funcionario.Sexo = Convert.ToChar(dr["Sexo"]);
                        funcionario.Email = dr["Email"].ToString();
                        funcionario.Telefone = Convert.ToInt64(dr["Telefone"]);
                        var cargo = dr["Cargo"].ToString();
                        funcionario.DataAdmissao = Convert.ToDateTime(dr["DataAdmissao"]);
                        funcionario.Endereco = new Endereco();
                        funcionario.Endereco.ID = Convert.ToInt32(dr["Endereco"]);
                        funcionario.Departamento = new Departamento();
                        funcionario.Departamento.ID = Convert.ToInt32(dr["Departamento"]);

                        switch (cargo)
                        {
                            case "Gerente":
                                funcionario.Cargo = EnumCargo.Gerente;
                                break;

                            case "Caixa":
                                funcionario.Cargo = EnumCargo.Caixa;
                                break;

                            case "Estoque":
                                funcionario.Cargo = EnumCargo.Estoque;
                                break;
                        }

                        lstFuncionario.Add(funcionario);
                    }

                    connection.Close();
                }
                return lstFuncionario;
            }
            catch (Exception)
            {
                throw;
            }
        }

        public bool Demitir(string cpf)
        {
            SqlTransaction sqlTransaction = null;

            try
            {
                Conexao objConexao = new Conexao();
                SqlCommand command = new SqlCommand();

                using (var connection = new SqlConnection(objConexao.getConexao()))
                {
                    command.Connection = connection;

                    connection.Open();

                    sqlTransaction = connection.BeginTransaction();

                    command.Transaction = sqlTransaction;

                    command.CommandText = @"UPDATE Funcionario
                                               SET DataDemissao = @DataDemissao
                                             WHERE CPF = @CPF";

                    command.Parameters.AddWithValue("@DataDemissao", DateTime.Now);
                    command.Parameters.AddWithValue("@CPF", cpf);

                    command.ExecuteNonQuery();

                    sqlTransaction.Commit();

                    connection.Close();

                    return true;
                }
            }
            catch (Exception)
            {
                sqlTransaction.Rollback();
                return false;
            }
        }

        public bool Readmitir(string cpf)
        {
            SqlTransaction sqlTransaction = null;

            try
            {
                Conexao objConexao = new Conexao();
                SqlCommand command = new SqlCommand();

                using (var connection = new SqlConnection(objConexao.getConexao()))
                {
                    command.Connection = connection;

                    connection.Open();

                    sqlTransaction = connection.BeginTransaction();

                    command.Transaction = sqlTransaction;

                    command.CommandText = @"UPDATE Funcionario
                                               SET DataDemissao = NULL
                                             WHERE CPF = @CPF";
                    
                    command.Parameters.AddWithValue("@CPF", cpf);
                    command.ExecuteNonQuery();

                    sqlTransaction.Commit();

                    connection.Close();

                    return true;
                }
            }
            catch (Exception)
            {
                sqlTransaction.Rollback();
                return false;
            }
        }

        public List<Funcionario> ListarTodos()
        {
            List<Funcionario> lstFuncionario = new List<Funcionario>();
            try
            {
                Conexao objConexao = new Conexao();
                SqlCommand command = new SqlCommand();

                using (var connection = new SqlConnection(objConexao.getConexao()))
                {
                    command.Connection = connection;

                    connection.Open();

                    command.CommandText = @"SELECT * FROM Funcionario";

                    SqlDataReader dr = command.ExecuteReader();

                    while (dr.Read())
                    {
                        Funcionario funcionario = new Funcionario();

                        funcionario.Nome = dr["Nome"].ToString();
                        funcionario.CPF = dr["CPF"].ToString();
                        funcionario.RG = dr["RG"].ToString();
                        funcionario.Sexo = Convert.ToChar(dr["Sexo"]);
                        funcionario.Email = dr["Email"].ToString();
                        funcionario.Telefone = Convert.ToInt64(dr["Telefone"]);
                        var cargo = dr["Cargo"].ToString();
                        funcionario.DataAdmissao = Convert.ToDateTime(dr["DataAdmissao"]);

                        string data = dr["DataDemissao"].ToString() == null ? null : dr["DataDemissao"].ToString();

                        if (!data.Equals(""))
                        {
                            funcionario.DataDemissao = Convert.ToDateTime(dr["DataDemissao"]);
                        }
                        else
                        {
                            funcionario.DataDemissao = null;
                        }

                        funcionario.Endereco = new Endereco();
                        funcionario.Endereco.ID = Convert.ToInt32(dr["Endereco"]);
                        funcionario.Departamento = new Departamento();
                        funcionario.Departamento.ID = Convert.ToInt32(dr["Departamento"]);

                        switch (cargo)
                        {
                            case "Gerente":
                                funcionario.Cargo = EnumCargo.Gerente;
                                break;

                            case "Caixa":
                                funcionario.Cargo = EnumCargo.Caixa;
                                break;

                            case "Estoque":
                                funcionario.Cargo = EnumCargo.Estoque;
                                break;
                        }

                        lstFuncionario.Add(funcionario);
                    }

                    connection.Close();
                }
                return lstFuncionario;
            }
            catch (Exception)
            {
                throw;
            }
        }

        public Funcionario ListarSelecionado(string CPF)
        {
            Funcionario funcionario = new Funcionario();
            try
            {
                Conexao objConexao = new Conexao();
                SqlCommand command = new SqlCommand();

                using (var connection = new SqlConnection(objConexao.getConexao()))
                {
                    command.Connection = connection;

                    connection.Open();

                    command.CommandText = @"SELECT * FROM Funcionario
                                                    WHERE CPF = @CPF;";

                    command.Parameters.AddWithValue("@CPF", CPF);

                    SqlDataReader dr = command.ExecuteReader();

                    var cargo = string.Empty;

                    dr.Read();

                    funcionario.Nome = dr["Nome"].ToString();
                    funcionario.CPF = dr["CPF"].ToString();
                    funcionario.RG = dr["RG"].ToString();
                    funcionario.Sexo = Convert.ToChar(dr["Sexo"]);
                    funcionario.Email = dr["Email"].ToString();
                    funcionario.Telefone = Convert.ToInt64(dr["Telefone"]);
                    cargo = dr["Cargo"].ToString();
                    funcionario.DataAdmissao = Convert.ToDateTime(dr["DataAdmissao"]);

                    string data = dr["DataDemissao"] == null ? null : dr["DataDemissao"].ToString();
                    if (!data.Equals(""))
                    {
                        funcionario.DataDemissao = Convert.ToDateTime(data);
                    }
                    else
                    {
                        funcionario.DataDemissao = null;
                    }

                    funcionario.Endereco = new Endereco();
                    funcionario.Endereco.ID = Convert.ToInt32(dr["Endereco"]);
                    funcionario.Departamento = new Departamento();
                    funcionario.Departamento.ID = Convert.ToInt32(dr["Departamento"]);

                    switch (cargo)
                    {
                        case "Gerente":
                            funcionario.Cargo = EnumCargo.Gerente;
                            break;

                        case "Caixa":
                            funcionario.Cargo = EnumCargo.Caixa;
                            break;

                        case "Estoque":
                            funcionario.Cargo = EnumCargo.Estoque;
                            break;
                    }

                    connection.Close();
                }
                return funcionario;
            }
            catch (Exception)
            {
                return funcionario = null;
            }
        }

        public bool AtualizarDados(Funcionario funcionario)
        {
            SqlTransaction sqlTransaction = null;

            try
            {
                Conexao objConexao = new Conexao();
                SqlCommand command = new SqlCommand();

                using (var connection = new SqlConnection(objConexao.getConexao()))
                {
                    command.Connection = connection;

                    connection.Open();

                    sqlTransaction = connection.BeginTransaction();

                    command.Transaction = sqlTransaction;

                    command.CommandText = @"UPDATE Funcionario
                                               SET Nome = @Nome,
                                                   RG = @RG,
                                                   Sexo = @Sexo,
                                                   Email = @Email,
                                                   Telefone = @Telefone,
                                                   Cargo = @Cargo,
                                                   DataAdmissao = @DataAdmissao,
                                                   Endereco = @Endereco,
                                                   Departamento = @Departamento
                                             WHERE CPF = @CPF";

                    command.Parameters.AddWithValue("@Nome", funcionario.Nome);
                    command.Parameters.AddWithValue("@RG", funcionario.RG);
                    command.Parameters.AddWithValue("@Sexo", funcionario.Sexo);
                    command.Parameters.AddWithValue("@Email", funcionario.Email);
                    command.Parameters.AddWithValue("@Telefone", funcionario.Telefone);
                    command.Parameters.AddWithValue("@Cargo", funcionario.Cargo);
                    command.Parameters.AddWithValue("@DataAdmissao", funcionario.DataAdmissao);
                    command.Parameters.AddWithValue("@Endereco", funcionario.Endereco.ID);
                    command.Parameters.AddWithValue("@Departamento", funcionario.Departamento.ID);
                    command.Parameters.AddWithValue("@CPF", funcionario.CPF);


                    command.ExecuteNonQuery();

                    sqlTransaction.Commit();

                    connection.Close();

                    return true;
                }
            }
            catch (Exception ex)
            {
                sqlTransaction.Rollback();
                return false;
            }
        }
    }
}