﻿using PfcWeb.Data.Connection;
using PfcWeb.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PfcWeb.Data.DAO
{
    public class VendaDAO
    {
        public bool CadastrarVenda(Venda venda)
        {
            SqlTransaction sqlTransaction = null;

            try
            {
                Conexao objConexao = new Conexao();
                SqlCommand command = new SqlCommand();

                using (var connection = new SqlConnection(objConexao.getConexao()))
                {
                    command.Connection = connection;

                    connection.Open();

                    sqlTransaction = connection.BeginTransaction();

                    command.Transaction = sqlTransaction;

                    command.CommandText = @"INSERT INTO Venda (DataHora,
                                                               Usuario,
                                                               TotalItens,
                                                               ValorTotal,
                                                               Recebido,
                                                               Troco,
                                                               Status)
                                                       VALUES (@DataHora,
                                                               @Usuario,
                                                               @TotalItens,
                                                               @ValorTotal,
                                                               @Recebido,
                                                               @Troco,
                                                               @Status)";

                    command.Parameters.AddWithValue("@DataHora", venda.DataHora);
                    command.Parameters.AddWithValue("@Usuario", venda.Usuario.ID);
                    command.Parameters.AddWithValue("@TotalItens", venda.TotalItens);
                    command.Parameters.AddWithValue("@ValorTotal", venda.ValorTotal);
                    command.Parameters.AddWithValue("@Recebido", venda.Recebido);
                    command.Parameters.AddWithValue("@Troco", venda.Troco);
                    command.Parameters.AddWithValue("@Status", venda.Status);

                    command.ExecuteNonQuery();

                    sqlTransaction.Commit();

                    connection.Close();

                    return true;
                }
            }
            catch (Exception)
            {
                sqlTransaction.Rollback();
                return false;
            }
        }

        public void DeletarVendaNaoFinalizada(Venda venda)
        {
            SqlTransaction sqlTransaction = null;

            try
            {
                Conexao objConexao = new Conexao();
                SqlCommand command = new SqlCommand();

                using (var connection = new SqlConnection(objConexao.getConexao()))
                {
                    command.Connection = connection;

                    connection.Open();

                    sqlTransaction = connection.BeginTransaction();

                    command.Transaction = sqlTransaction;

                    command.CommandText = @"DELETE FROM Venda
                                                  WHERE ID = @ID";

                    command.Parameters.AddWithValue("@ID", venda.ID);

                    command.ExecuteNonQuery();

                    sqlTransaction.Commit();

                    connection.Close();
                }
            }
            catch (Exception)
            {
                sqlTransaction.Rollback();
                throw;
            }
        }

        public List<Venda> GetVendasNaoFinalizadas()
        {
            List<Venda> lstVenda = new List<Venda>();
            try
            {
                Conexao objConexao = new Conexao();
                SqlCommand command = new SqlCommand();

                using (var connection = new SqlConnection(objConexao.getConexao()))
                {
                    command.Connection = connection;

                    connection.Open();

                    command.CommandText = @"SELECT * FROM Venda
                                                    WHERE Status = 'NÃO FINALIZADO'";

                    SqlDataReader dr = command.ExecuteReader();

                    while (dr.Read())
                    {
                        Venda venda = new Venda()
                        {
                            ID = Convert.ToInt32(dr["ID"]),
                            DataHora = Convert.ToDateTime(dr["DataHora"]),
                            Recebido = Convert.ToDouble(dr["Recebido"]),
                            TotalItens = Convert.ToInt32(dr["TotalItens"]),
                            Troco = Convert.ToDouble(dr["Troco"]),
                            ValorTotal = Convert.ToDouble(dr["ValorTotal"]),
                            Status = dr["Status"].ToString()
                            
                        };
                        venda.Usuario = new Usuario();

                        venda.Usuario.ID = Convert.ToInt32(dr["Usuario"]);

                        lstVenda.Add(venda);
                    }

                    connection.Close();
                }
                return lstVenda;
            }
            catch (Exception)
            {
                throw;
            }
        }

        public int GetLastIdVenda(int usuarioId)
        {
            int id = 0;
            try
            {
                Conexao objConexao = new Conexao();
                SqlCommand command = new SqlCommand();

                using (var connection = new SqlConnection(objConexao.getConexao()))
                {
                    command.Connection = connection;

                    connection.Open();

                    command.CommandText = @"SELECT TOP 1 ID FROM Venda
                                                           WHERE Usuario = @Usuario
                                                        ORDER BY ID DESC;";

                    command.Parameters.AddWithValue("@Usuario", usuarioId);

                    SqlDataReader dr = command.ExecuteReader();

                    dr.Read();

                    id = Convert.ToInt32(dr["ID"]);

                    connection.Close();
                }
                return id;
            }
            catch (Exception)
            {
                throw;
            }
        }

        public bool FinalizarVenda(Venda venda, int id)
        {
            SqlTransaction sqlTransaction = null;

            try
            {
                Conexao objConexao = new Conexao();
                SqlCommand command = new SqlCommand();

                using (var connection = new SqlConnection(objConexao.getConexao()))
                {
                    command.Connection = connection;

                    connection.Open();

                    sqlTransaction = connection.BeginTransaction();

                    command.Transaction = sqlTransaction;

                    command.CommandText = @"UPDATE Venda
                                               SET DataHora = @DataHora,
                                                 TotalItens = @TotalItens,
                                                 ValorTotal = @ValorTotal,
                                                   Recebido = @Recebido,
                                                      Troco = @Troco,
                                                     Status = @Status
                                             WHERE ID = @ID";

                    command.Parameters.AddWithValue("@DataHora", DateTime.Now);
                    command.Parameters.AddWithValue("@TotalItens", venda.TotalItens);
                    command.Parameters.AddWithValue("@ValorTotal", venda.ValorTotal);
                    command.Parameters.AddWithValue("@Recebido", venda.Recebido);
                    command.Parameters.AddWithValue("@Troco", venda.Troco);
                    command.Parameters.AddWithValue("@Status", venda.Status);
                    command.Parameters.AddWithValue("@ID", id);
                    command.ExecuteNonQuery();

                    sqlTransaction.Commit();

                    connection.Close();

                    return true;
                }
            }
            catch (Exception)
            {
                sqlTransaction.Rollback();
                return false;
            }
        }

        public List<Venda> ListarTodos()
        {
            List<Venda> lstVenda = new List<Venda>();
            try
            {
                Conexao objConexao = new Conexao();
                SqlCommand command = new SqlCommand();

                using (var connection = new SqlConnection(objConexao.getConexao()))
                {
                    command.Connection = connection;

                    connection.Open();

                    command.CommandText = @"SELECT * FROM Venda
                                                    WHERE Status = 'FINALIZADO'
                                                    ORDER BY DataHora;";

                    SqlDataReader dr = command.ExecuteReader();

                    while (dr.Read())
                    {
                        Venda venda = new Venda();

                        venda.ID = Convert.ToInt32(dr["ID"]);
                        venda.DataHora = Convert.ToDateTime(dr["DataHora"]);
                        venda.ValorTotal = Convert.ToDouble(dr["ValorTotal"]);

                        lstVenda.Add(venda);
                    }

                    connection.Close();
                }
                return lstVenda;
            }
            catch (Exception)
            {
                throw;
            }
        }

        public List<Venda> RelatorioVendasPorPeriodo(DateTime date1, DateTime date2)
        {
            List<Venda> lstVenda = new List<Venda>();
            try
            {
                Conexao objConexao = new Conexao();
                SqlCommand command = new SqlCommand();

                using (var connection = new SqlConnection(objConexao.getConexao()))
                {
                    command.Connection = connection;

                    connection.Open();

                    command.CommandText = @"SELECT * FROM Venda
                                                    WHERE DataHora BETWEEN @Date1 AND @Date2
                                                      AND Status = 'FINALIZADO'
                                                    ORDER BY DataHora;";

                    command.Parameters.AddWithValue("@Date1", date1);
                    command.Parameters.AddWithValue("@Date2", date2);

                    SqlDataReader dr = command.ExecuteReader();

                    while (dr.Read())
                    {
                        Venda venda = new Venda();

                        venda.ID = Convert.ToInt32(dr["ID"]);
                        venda.DataHora = Convert.ToDateTime(dr["DataHora"]);
                        venda.ValorTotal = Convert.ToDouble(dr["ValorTotal"]);
                        venda.TotalItens = Convert.ToInt32(dr["TotalItens"]);

                        lstVenda.Add(venda);
                    }

                    connection.Close();
                }
                return lstVenda;
            }
            catch (Exception)
            {
                throw;
            }
        }

        public List<Venda> RelatorioVendasAPartirDe(DateTime date1)
        {
            List<Venda> lstVenda = new List<Venda>();
            try
            {
                Conexao objConexao = new Conexao();
                SqlCommand command = new SqlCommand();

                using (var connection = new SqlConnection(objConexao.getConexao()))
                {
                    command.Connection = connection;

                    connection.Open();

                    command.CommandText = @"SELECT * FROM Venda
                                                    WHERE DataHora >= @Date1
                                                      AND Status = 'FINALIZADO'
                                                    ORDER BY DataHora;";

                    command.Parameters.AddWithValue("@Date1", date1);

                    SqlDataReader dr = command.ExecuteReader();

                    while (dr.Read())
                    {
                        Venda venda = new Venda();

                        venda.ID = Convert.ToInt32(dr["ID"]);
                        venda.DataHora = Convert.ToDateTime(dr["DataHora"]);
                        venda.ValorTotal = Convert.ToDouble(dr["ValorTotal"]);
                        venda.TotalItens = Convert.ToInt32(dr["TotalItens"]);
                        venda.Status = dr["Status"].ToString();

                        lstVenda.Add(venda);
                    }

                    connection.Close();
                }
                return lstVenda;
            }
            catch (Exception)
            {
                throw;
            }
        }

        public List<Venda> RelatorioVendasAteQue(DateTime date2)
        {
            List<Venda> lstVenda = new List<Venda>();
            try
            {
                Conexao objConexao = new Conexao();
                SqlCommand command = new SqlCommand();

                using (var connection = new SqlConnection(objConexao.getConexao()))
                {
                    command.Connection = connection;

                    connection.Open();

                    command.CommandText = @"SELECT * FROM Venda
                                                    WHERE DataHora <= @Date2
                                                      AND Status = 'FINALIZADO'
                                                    ORDER BY DataHora;";
                    
                    command.Parameters.AddWithValue("@Date2", date2);

                    SqlDataReader dr = command.ExecuteReader();

                    while (dr.Read())
                    {
                        Venda venda = new Venda();

                        venda.ID = Convert.ToInt32(dr["ID"]);
                        venda.DataHora = Convert.ToDateTime(dr["DataHora"]);
                        venda.ValorTotal = Convert.ToDouble(dr["ValorTotal"]);
                        venda.TotalItens = Convert.ToInt32(dr["TotalItens"]);

                        lstVenda.Add(venda);
                    }

                    connection.Close();
                }
                return lstVenda;
            }
            catch (Exception)
            {
                throw;
            }
        }
    }
}