﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using PfcWeb.Domain.Entities;
using System.Data.SqlClient;
using PfcWeb.Data.Connection;

namespace PfcWeb.Data.DAO
{
    public class DepartamentoDAO
    {
        public bool Cadastrar(Departamento departamento)
        {
            SqlTransaction sqlTransaction = null;

            try
            {
                Conexao objConexao = new Conexao();
                SqlCommand command = new SqlCommand();

                using (var connection = new SqlConnection(objConexao.getConexao()))
                {
                    command.Connection = connection;

                    connection.Open();

                    sqlTransaction = connection.BeginTransaction();

                    command.Transaction = sqlTransaction;

                    command.CommandText = @"INSERT INTO Departamento (Nome,
                                                                      Descricao,
                                                                      Status)
                                                             VALUES (@Nome,
                                                                     @Descricao,
                                                                     1)";

                    command.Parameters.AddWithValue("@Nome", departamento.Nome);
                    command.Parameters.AddWithValue("@Descricao", departamento.Descricao);

                    command.ExecuteNonQuery();

                    sqlTransaction.Commit();

                    connection.Close();

                    return true;
                }
            }
            catch (Exception)
            {
                sqlTransaction.Rollback();
                return false;
            }
        }

        public bool Desativar(int id)
        {
            SqlTransaction sqlTransaction = null;

            try
            {
                Conexao objConexao = new Conexao();
                SqlCommand command = new SqlCommand();

                using (var connection = new SqlConnection(objConexao.getConexao()))
                {
                    command.Connection = connection;

                    connection.Open();

                    sqlTransaction = connection.BeginTransaction();

                    command.Transaction = sqlTransaction;

                    command.CommandText = @"UPDATE Departamento
                                               SET Status = 0
                                             WHERE ID = @ID";

                    command.Parameters.AddWithValue("@ID", id);
                    command.ExecuteNonQuery();

                    sqlTransaction.Commit();

                    connection.Close();

                    return true;
                }
            }
            catch (Exception)
            {
                sqlTransaction.Rollback();
                return false;
            }
        }

        public List<Departamento> ListarTodos()
        {
            List<Departamento> lstDepartamento = new List<Departamento>();
            try
            {
                Conexao objConexao = new Conexao();
                SqlCommand command = new SqlCommand();

                using (var connection = new SqlConnection(objConexao.getConexao()))
                {
                    command.Connection = connection;

                    connection.Open();

                    command.CommandText = @"SELECT * FROM Departamento
                                                    WHERE Status = 1
                                                    ORDER BY ID;";

                    SqlDataReader dr = command.ExecuteReader();

                    while (dr.Read())
                    {
                        Departamento departamento = new Departamento();

                        departamento.ID = Convert.ToInt32(dr["ID"]);
                        departamento.Nome = dr["Nome"].ToString();
                        departamento.Descricao = dr["Descricao"].ToString();
                        departamento.Status = Convert.ToBoolean(dr["Status"]);

                        lstDepartamento.Add(departamento);
                    }

                    connection.Close();
                }
                return lstDepartamento;
            }
            catch (Exception)
            {
                throw;
            }
        }

        public Departamento ListarSelecionado(int id)
        {
            Departamento departamento = new Departamento();
            try
            {
                Conexao objConexao = new Conexao();
                SqlCommand command = new SqlCommand();

                using (var connection = new SqlConnection(objConexao.getConexao()))
                {
                    command.Connection = connection;

                    connection.Open();

                    command.CommandText = @"SELECT * FROM Departamento
                                                    WHERE ID = @ID
                                                    ORDER BY ID;";

                    command.Parameters.AddWithValue("@ID", id);

                    SqlDataReader dr = command.ExecuteReader();

                    dr.Read();

                    departamento.ID = Convert.ToInt32(dr["ID"]);
                    departamento.Nome = dr["Nome"].ToString();
                    departamento.Descricao = dr["Descricao"].ToString();
                    departamento.Status = Convert.ToBoolean(dr["Status"]);

                    connection.Close();
                }
                return departamento;
            }
            catch (Exception)
            {
                return departamento = null;
            }
        }

        public Departamento ListarSelecionadoNome(string nome)
        {
            Departamento departamento = new Departamento();
            try
            {
                Conexao objConexao = new Conexao();
                SqlCommand command = new SqlCommand();

                using (var connection = new SqlConnection(objConexao.getConexao()))
                {
                    command.Connection = connection;

                    connection.Open();

                    command.CommandText = @"SELECT * FROM Departamento
                                                    WHERE Nome = @Nome
                                                    ORDER BY ID;";

                    command.Parameters.AddWithValue("@Nome", nome);

                    SqlDataReader dr = command.ExecuteReader();

                    dr.Read();

                    departamento.ID = Convert.ToInt32(dr["ID"]);
                    departamento.Nome = dr["Nome"].ToString();
                    departamento.Descricao = dr["Descricao"].ToString();
                    departamento.Status = Convert.ToBoolean(dr["Status"]);

                    connection.Close();
                }
                return departamento;
            }
            catch (Exception)
            {
                return departamento = null;
            }
        }

        public bool AtualizarDados(Departamento departamento)
        {
            SqlTransaction sqlTransaction = null;

            try
            {
                Conexao objConexao = new Conexao();
                SqlCommand command = new SqlCommand();

                using (var connection = new SqlConnection(objConexao.getConexao()))
                {
                    command.Connection = connection;

                    connection.Open();

                    sqlTransaction = connection.BeginTransaction();

                    command.Transaction = sqlTransaction;

                    command.CommandText = @"UPDATE Departamento
                                               SET Nome = @Nome,
                                                   Descricao = @Descricao
                                             WHERE ID = @ID";

                    command.Parameters.AddWithValue("@Nome", departamento.Nome);
                    command.Parameters.AddWithValue("@Descricao", departamento.Descricao);
                    command.Parameters.AddWithValue("@ID", departamento.ID);
                    command.ExecuteNonQuery();

                    sqlTransaction.Commit();

                    connection.Close();

                    return true;
                }
            }
            catch (Exception)
            {
                sqlTransaction.Rollback();
                return false;
            }
        }
    }
}