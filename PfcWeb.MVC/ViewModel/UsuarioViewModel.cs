﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PfcWeb.MVC.ViewModel
{
    public class UsuarioViewModel
    {
        public int ID { get; set; }
        public string Login { get; set; }
        public string Senha { get; set; }
        public string NivelAcesso { get; set; }
        public string Status { get; set; }
    }
}