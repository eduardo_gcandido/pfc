﻿using Hangfire;
using Microsoft.Owin;
using Owin;
using PFC.Data.DAO;
using PfcWeb.Data.Connection;
using PfcWeb.Data.DAO;
using PfcWeb.Domain.Entities;
using PfcWeb.MVC.Controllers;
using System;
using System.Collections.Generic;
using System.Net.Mail;

[assembly: OwinStartup(typeof(MyWebApplication.Startup))]

namespace MyWebApplication
{
    public class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            Conexao conexao = new Conexao();

            GlobalConfiguration.Configuration.UseSqlServerStorage(conexao.getConexao());

            app.UseHangfireDashboard();
            app.UseHangfireServer();

            RecurringJob.AddOrUpdate(() => ControleDeVendas(), Cron.Daily());

            RecurringJob.AddOrUpdate(() => EnviaEmailDescarte(), Cron.Daily());

            RecurringJob.AddOrUpdate(() => EnviaEmailReporPrateleira(), Cron.HourInterval(1));
        }

        public void ControleDeVendas()
        {
            VendaController vendaController = new VendaController();

            vendaController.ControleDeVendasNaoFinalizada();
        }

        public void EnviaEmailDescarte()
        {
            LoteController loteController = new LoteController();

            loteController.EnviaEmailDescarte();
        }

        public void EnviaEmailReporPrateleira()
        {
            PrateleiraController prateleiraController = new PrateleiraController();

            prateleiraController.EnviaEmailReporPrateleira();
        }

    }
}