﻿using PagedList;
using PfcWeb.Data.DAO;
using PfcWeb.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Drawing.Printing;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace PfcWeb.MVC.Controllers
{
    [Authorize(Roles = "GERENTE")]
    public class DepartamentoController : Controller
    {
        // GET: CadastrarDepartamento
        public ActionResult CadastrarDepartamento()
        {
            return View();
        }

        [HttpPost]
        public ActionResult CadastrarDepartamento(Departamento departamento)
        {
            if (ModelState.IsValid)
            {
                departamento.Nome = departamento.Nome.ToUpper();

                DepartamentoDAO dao = new DepartamentoDAO();

                var departamentoSelecionado = dao.ListarSelecionadoNome(departamento.Nome);

                if (departamentoSelecionado == null)
                {
                    if (dao.Cadastrar(departamento))
                    {
                        return RedirectToAction("ListaDepartamento", "Departamento");
                    }
                }
                else
                {
                    TempData["Message"] = string.Format("Departamento já cadastrado");
                }
            }

            return View();
        }
        
        public ActionResult ListaDepartamento()
        {
            DepartamentoDAO dao = new DepartamentoDAO();

            return View(dao.ListarTodos());
        }
        
        public ActionResult EditarDepartamento(int id)
        {
            if (id == 0)
            {
                return RedirectToAction("ListaDepartamento", "Departamento");
            }
            else
            {
                DepartamentoDAO dao = new DepartamentoDAO();

                Departamento departamento = dao.ListarSelecionado(id);

                return View(departamento);
            }
        }
        
        [HttpPost]
        public ActionResult EditarDepartamento(Departamento departamento)
        {
            DepartamentoDAO dao = new DepartamentoDAO();

            if (dao.AtualizarDados(departamento))
            {
                return RedirectToAction("ListaDepartamento", "Departamento");
            }
            else
            {
                TempData["Message"] = string.Format("Erro ao efetuar a atualização");
                return View(departamento);
            }
        }
    }
}