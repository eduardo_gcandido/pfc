﻿using PFC.Data.DAO;
using PfcWeb.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace PfcWeb.MVC.Controllers
{
    [Authorize(Roles = "ESTOQUE")]
    public class CategoriaController : Controller
    {
        // GET: CadastrarCategoria
        public ActionResult CadastrarCategoria()
        {
            var categoria = new Categoria();
            return View(categoria);
        }

        [HttpPost]
        public ActionResult CadastrarCategoria(Categoria categoria)
        {
            categoria.Descricao = categoria.Descricao.ToUpper();

            CategoriaDAO dao = new CategoriaDAO();

            var categoriaSelecionada = dao.ListarSelecionadoNome(categoria.Descricao);

            if (categoriaSelecionada == null)
            {
                if (dao.Cadastrar(categoria))
                {
                    return RedirectToAction("ListaCategoria", "Categoria");
                }
            }
            else
            {
                TempData["Message"] = string.Format("Categoria já cadastrado");
            }

            return View();
        }
        
        public ActionResult ListaCategoria()
        {
            CategoriaDAO dao = new CategoriaDAO();

            return View(dao.ListarTodos());
        }
        
        public ActionResult EditarCategoria(int id)
        {
            if (id == 0)
            {
                return RedirectToAction("ListaCategoria", "Categoria");
            }
            else
            {
                CategoriaDAO dao = new CategoriaDAO();

                Categoria categoria = dao.ListarSelecionadoID(id);

                return View(categoria);
            }
        }
        
        [HttpPost]
        public ActionResult EditarCategoria(Categoria categoria)
        {
            CategoriaDAO dao = new CategoriaDAO();

            categoria.Descricao = categoria.Descricao.ToUpper();

            if (dao.AtualizarDados(categoria))
            {
                return RedirectToAction("ListaCategoria", "Categoria");
            }
            else
            {
                TempData["Message"] = string.Format("Erro ao efetuar a atualização");
                return View(categoria);
            }
        }
    }
}