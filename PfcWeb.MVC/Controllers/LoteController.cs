﻿using PfcWeb.Domain.Entities;
using PfcWeb.Data.DAO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using PFC.Data.DAO;
using PfcWeb.MVC.ViewModel;
using System.Net.Mail;

namespace PfcWeb.MVC.Controllers
{
    [Authorize(Roles = "ESTOQUE")]
    public class LoteController : Controller
    {
        // GET: Lote
        public ActionResult CadastrarLote()
        {
            var lote = new Lote();

            CategoriaDAO categoriaDAO = new CategoriaDAO();
            ProdutoDAO produtoDAO = new ProdutoDAO();

            ViewBag.Categoria = categoriaDAO.ListarTodos();

            ViewBag.Produto = produtoDAO.ListarTodos();

            if (ViewBag.Categoria.Count > 0)
            {
                if (ViewBag.Produto.Count > 0)
                {
                    return View(lote);
                }
                else
                {
                    TempData["Message"] = string.Format("Não existem Produtos cadastrados, favor cadastrar");
                    return RedirectToAction("ConsultaLote", "Lote");
                }

            }
            else
            {
                TempData["Message"] = string.Format("Não existem categorias cadastrados, favor cadastrar");
                return RedirectToAction("ConsultaLote", "Lote");
            }
        }

        [HttpPost]
        public ActionResult CadastrarLote(Lote lote)
        {
            LoteDAO dao = new LoteDAO();

            if (lote.DataValidade > DateTime.Now)
            {
                Lote aux = dao.ListarSelecionadoBarcode(lote.Barcode);

                if (aux == null)
                {
                    if (dao.Cadastrar(lote))
                    {
                        return RedirectToAction("ConsultaLote", "Lote");
                    }
                }
                else
                {
                    CategoriaDAO categoriaDAO = new CategoriaDAO();
                    ProdutoDAO produtoDAO = new ProdutoDAO();

                    ViewBag.Categoria = categoriaDAO.ListarTodos();

                    ViewBag.Produto = produtoDAO.ListarTodos();

                    TempData["Message"] = string.Format("Já existe um lote com esse código de barras");
                    return View(lote);
                }
            }
            else
            {
                CategoriaDAO categoriaDAO = new CategoriaDAO();
                ProdutoDAO produtoDAO = new ProdutoDAO();

                ViewBag.Categoria = categoriaDAO.ListarTodos();

                ViewBag.Produto = produtoDAO.ListarTodos();

                TempData["Message"] = string.Format("Data de validade não pode ser menor que a data de hoje");
                return View(lote);
            }


            return View(lote);
        }
        
        public ActionResult ConsultaLote()
        {
            LoteDAO loteDAO = new LoteDAO();
            ProdutoDAO produtoDAO = new ProdutoDAO();
            CategoriaDAO categoriaDAO = new CategoriaDAO();

            List<Lote> list = new List<Lote>();

            list = loteDAO.ListarTodos();

            for (var i = 0; i < list.Count; i++)
            {
                list[i].Categoria = categoriaDAO.ListarSelecionadoID(list[i].Categoria.ID);

                list[i].DataValidade = Convert.ToDateTime(list[i].DataValidade.ToString("dd/MM/yyyy"));

                list[i].DataEntrada = Convert.ToDateTime(list[i].DataEntrada.ToString("dd/MM/yyyy"));

                list[i].Produto = produtoDAO.ListarSelecionadoID(list[i].Produto.ID);
            }

            return View(list);
        }
        
        public ActionResult EditarLote(int id)
        {
            if (id == 0)
            {
                return RedirectToAction("ConsultaLote", "Lote");
            }
            else
            {
                LoteDAO dao = new LoteDAO();
                ProdutoDAO produtoDAO = new ProdutoDAO();
                CategoriaDAO categoriaDAO = new CategoriaDAO();

                Lote lote = new Lote();
                lote.Categoria = new Categoria();
                lote.Produto = new Produto();

                lote = dao.ListarSelecionadoID(id);

                ViewBag.Categoria = categoriaDAO.ListarTodos();
                ViewBag.Produto = produtoDAO.ListarTodos();

                return View(lote);
            }
        }

        [HttpPost]
        public ActionResult EditarLote(Lote lote)
        {
            LoteDAO dao = new LoteDAO();

            if (dao.AtualizarDados(lote))
            {
                return RedirectToAction("ConsultaLote", "Lote");
            }
            else
            {
                TempData["Message"] = string.Format("Erro ao efetuar a atualização");
                return View(lote);
            }
        }
        
        public ActionResult DescartarLote(int id)
        {
            LoteDAO dao = new LoteDAO();

            if (dao.DesativarLote(id))
            {
                return RedirectToAction("ConsultaLote", "Lote");
            }
            else
            {
                return View();
            }
        }
        
        public JsonResult TransferirProdutos(int id, string quantidade)
        {
            HttpCookie cookie = Request.Cookies.Get("UsuarioID");

            LoteDAO loteDAO = new LoteDAO();

            PrateleiraDAO prateleiraDAO = new PrateleiraDAO();

            Lote lote = loteDAO.ListarSelecionadoID(Convert.ToInt32(id));

            var quantidadeNovaLote = lote.QuantidadeItens - Convert.ToInt64(quantidade);

            if (quantidadeNovaLote >= 0)
            {
                Prateleira prateleira = prateleiraDAO.ListarSelecionadoIDLote(lote.ID);

                if (prateleira != null)
                {
                    if (loteDAO.AtualizarQuantidade(lote.ID, quantidadeNovaLote))
                    {
                        var quantidadeNovaProduto = prateleira.Quantidade + Convert.ToInt32(quantidade);

                        prateleiraDAO.AtualizarQuantidade(prateleira.ID, quantidadeNovaProduto);

                        return Json(new
                        {
                            mensagem = "SUCESSO"
                        });
                    }
                    else
                    {
                        return Json(new
                        {
                            mensagem = "ERRO"
                        });
                    }
                }
                else
                {
                    return Json(new
                    {
                        mensagem = "ERRO PRATELEIRA"
                    });
                }
            }
            else
            {
                return Json(new
                {
                    mensagem = "ERRO QUANTIDADE"
                });
            }
        }

        public Lote GetLoteByBarcode(string barcode)
        {
            LoteDAO loteDAO = new LoteDAO();

            return loteDAO.ListarSelecionadoBarcode(barcode);
        }

        public void EnviaEmailDescarte()
        {
            LoteDAO loteDAO = new LoteDAO();

            List<Lote> listLote = loteDAO.ListarVencidos();

            if (listLote != null)
            {
                FuncionarioDAO funcionarioDAO = new FuncionarioDAO();

                List<Funcionario> listFuncionario = funcionarioDAO.ListarfuncionarioEstoque();

                if (listFuncionario != null)
                {
                    foreach (var item in listFuncionario)
                    {
                        string lotesId = "";

                        foreach (var lote in listLote)
                        {
                            lotesId = lotesId + lote.ID + ",";
                        }

                        if (lotesId.Length > 0)
                        {
                            lotesId = lotesId.Remove(lotesId.Length - 1);
                        }

                        if (listLote.Count > 0)
                        {
                            Email mail = new Email()
                            {
                                Sender = "prime.stock.tcc@gmail.com",
                                To = item.Email,
                                subject = "Lote Vencido",
                                Body = "Os lotes com os ID's " + lotesId + " estão vencidos, favor acessar o menu de Lotes e descarta-los"
                            };


                            try
                            {
                                //Instância smtp do servidor, neste caso o gmail.
                                SmtpClient smtp = new SmtpClient();
                                smtp.Host = "smtp.gmail.com";
                                smtp.Port = 587;
                                smtp.UseDefaultCredentials = false;
                                smtp.Credentials = new System.Net.NetworkCredential
                                ("prime.stock.tcc@gmail.com", "PrimeStock.2018");// Login e senha do e-mail.
                                smtp.EnableSsl = true;
                                smtp.Send(mail.Sender, mail.To, mail.subject, mail.Body);
                            }
                            catch (Exception ex)
                            {
                                throw new Exception(ex.Message);
                            }
                        }
                    }
                }
            }
        }
    }
}