﻿using PFC.Data.DAO;
using PfcWeb.Data.DAO;
using PfcWeb.Domain.Entities;
using PfcWeb.MVC.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;

namespace PfcWeb.MVC.Controllers
{
    [Authorize(Roles = "ESTOQUE")]
    public class ProdutoController : Controller
    {
        public ActionResult CadastrarProduto()
        {
            var produto = new Produto();

            return View(produto);
        }

        [HttpPost]
        public ActionResult CadastrarProduto(Produto produto)
        {
            ProdutoDAO dao = new ProdutoDAO();

            if (dao.Cadastrar(produto))
            {
                return RedirectToAction("ListaProduto", "Produto");
            }

            return View(produto);
        }

        public ActionResult ListaProduto()
        {
            ProdutoDAO produtoDAO = new ProdutoDAO();
            CategoriaDAO categoriaDAO = new CategoriaDAO();

            List<Produto> list = new List<Produto>();

            list = produtoDAO.ListarTodos();

            return View(list);
        }

        public ActionResult EditarProduto(int id)
        {
            if (id == 0)
            {
                return RedirectToAction("ListaProduto", "Produto");
            }
            else
            {
                ProdutoDAO dao = new ProdutoDAO();

                Produto produto = new Produto();

                produto = dao.ListarSelecionadoID(id);

                return View(produto);
            }
        }

        [HttpPost]
        public ActionResult EditarProduto(Produto produto)
        {
            ProdutoDAO dao = new ProdutoDAO();

            if (dao.AtualizarDados(produto))
            {
                return RedirectToAction("ListaProduto", "Produto");
            }
            else
            {
                TempData["Message"] = string.Format("Erro ao efetuar a atualização");
                return View(produto);
            }
        }
    }
}