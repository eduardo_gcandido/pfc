﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using PFC.Data.DAO;
using PfcWeb.Data.DAO;
using PfcWeb.Domain.Entities;

namespace PfcWeb.MVC.Controllers
{
    [Authorize(Roles = "GERENTE")]
    public class RelatoriosController : Controller
    {
        // GET: Relatorios
        public ActionResult SelecaoRelatorios()
        {
            return View();
        }
        
        public ActionResult VendasPeriodo()
        {
            return View();
        }
        
        [HttpPost]
        public ActionResult VendasPeriodo(string date1, string date2)
        {
            DateTime dateFirst = DateTime.Now;
            DateTime dateSecond = DateTime.Now;

            if (!date1.Equals(""))
            {
                dateFirst = Convert.ToDateTime(date1);
            }
            if (!date2.Equals(""))
            {
                dateSecond = Convert.ToDateTime(date2);
            }

            VendaDAO vendaDAO = new VendaDAO();

            if (!date1.Equals("") && !date2.Equals(""))
            {
                if (dateFirst <= dateSecond)
                {
                    return View(vendaDAO.RelatorioVendasPorPeriodo(dateFirst, dateSecond));
                }
                else
                {
                    TempData["Message"] = string.Format("A primeira data deve ser menor que a segunda");
                    return View();
                }
            }
            else if (!date1.Equals(""))
            {
                return View(vendaDAO.RelatorioVendasAPartirDe(dateFirst));
            }
            else if (!date2.Equals(""))
            {
                return View(vendaDAO.RelatorioVendasAteQue(dateSecond));
            }
            else
            {
                return View(vendaDAO.RelatorioVendasPorPeriodo(DateTime.Now, DateTime.Now));
            }
        }
        
        public ActionResult ProdutosVendidos()
        {
            return View();
        }
        
        [HttpPost]
        public ActionResult ProdutosVendidos(string date1, string date2)
        {
            DateTime dateFirst = DateTime.Now;
            DateTime dateSecond = DateTime.Now;

            if (!date1.Equals(""))
            {
                dateFirst = Convert.ToDateTime(date1);
            }
            if (!date2.Equals(""))
            {
                dateSecond = Convert.ToDateTime(date2);
            }

            VendaDAO vendaDAO = new VendaDAO();

            if (!date1.Equals("") && !date2.Equals(""))
            {
                if (dateFirst <= dateSecond)
                {
                    List<Venda> listVenda = vendaDAO.RelatorioVendasPorPeriodo(dateFirst, dateSecond);

                    List<ItemVenda> listItemVenda = new List<ItemVenda>();
                    ItemVendaDAO itemVendaDAO = new ItemVendaDAO();

                    foreach (var item in listVenda)
                    {
                        List<ItemVenda> listItemVendaAux = itemVendaDAO.ListarPorIDVenda(item);

                        foreach (var itemAux in listItemVendaAux)
                        {
                            itemAux.Venda = item;
                            listItemVenda.Add(itemAux);

                        }
                    }

                    LoteDAO loteDAO = new LoteDAO();
                    ProdutoDAO produtoDAO = new ProdutoDAO();

                    foreach (var item in listItemVenda)
                    {
                        item.Lote = loteDAO.ListarSelecionadoID(item.Lote.ID);

                        item.Lote.Produto = produtoDAO.ListarSelecionadoID(item.Lote.Produto.ID);
                    }

                    return View(listItemVenda);
                }
                else
                {
                    TempData["Message"] = string.Format("A primeira data deve ser menor que a segunda");
                    return View();
                }
            }
            else if (!date1.Equals(""))
            {
                List<Venda> listVenda = vendaDAO.RelatorioVendasAPartirDe(dateFirst);

                List<ItemVenda> listItemVenda = new List<ItemVenda>();
                ItemVendaDAO itemVendaDAO = new ItemVendaDAO();

                foreach (var item in listVenda)
                {
                    List<ItemVenda> listItemVendaAux = itemVendaDAO.ListarPorIDVenda(item);

                    listItemVendaAux[0].Venda = item;
                    listItemVenda.Add(listItemVendaAux[0]);

                    foreach (var itemAux in listItemVendaAux)
                    {
                        itemAux.Venda = item;
                        listItemVenda.Add(itemAux);

                    }

                }

                LoteDAO loteDAO = new LoteDAO();

                ProdutoDAO produtoDAO = new ProdutoDAO();

                foreach (var item in listItemVenda)
                {
                    item.Lote = loteDAO.ListarSelecionadoID(item.Lote.ID);

                    item.Lote.Produto = produtoDAO.ListarSelecionadoID(item.Lote.Produto.ID);
                }

                return View(listItemVenda);
            }
            else if (!date2.Equals(""))
            {
                List<Venda> listVenda = vendaDAO.RelatorioVendasAteQue(dateSecond);

                List<ItemVenda> listItemVenda = new List<ItemVenda>();
                ItemVendaDAO itemVendaDAO = new ItemVendaDAO();

                foreach (var item in listVenda)
                {
                    List<ItemVenda> listItemVendaAux = itemVendaDAO.ListarPorIDVenda(item);

                    foreach (var itemAux in listItemVendaAux)
                    {
                        itemAux.Venda = item;
                        listItemVenda.Add(itemAux);

                    }
                }

                LoteDAO loteDAO = new LoteDAO();

                ProdutoDAO produtoDAO = new ProdutoDAO();

                foreach (var item in listItemVenda)
                {
                    item.Lote = loteDAO.ListarSelecionadoID(item.Lote.ID);

                    item.Lote.Produto = produtoDAO.ListarSelecionadoID(item.Lote.Produto.ID);
                }

                return View(listItemVenda);
            }
            else
            {
                List<Venda> listVenda = vendaDAO.RelatorioVendasPorPeriodo(DateTime.Now, DateTime.Now);

                List<ItemVenda> listItemVenda = new List<ItemVenda>();
                ItemVendaDAO itemVendaDAO = new ItemVendaDAO();

                foreach (var item in listVenda)
                {
                    List<ItemVenda> listItemVendaAux = itemVendaDAO.ListarPorIDVenda(item);

                    foreach (var itemAux in listItemVendaAux)
                    {
                        itemAux.Venda = item;
                        listItemVenda.Add(itemAux);

                    }
                }

                LoteDAO loteDAO = new LoteDAO();

                ProdutoDAO produtoDAO = new ProdutoDAO();

                foreach (var item in listItemVenda)
                {
                    item.Lote = loteDAO.ListarSelecionadoID(item.Lote.ID);

                    item.Lote.Produto = produtoDAO.ListarSelecionadoID(item.Lote.Produto.ID);
                }

                return View(listItemVenda);
            }
        }
        
        public ActionResult SelecionarCategoriaLotes()
        {
            Lote lote = new Lote();

            CategoriaDAO categoriaDAO = new CategoriaDAO();

            ViewBag.Categoria = categoriaDAO.ListarTodos();

            return View(lote);
        }
        
        [HttpPost]
        public ActionResult LotesDescartados(Categoria categoria)
        {
            LoteDAO loteDAO = new LoteDAO();

            List<Lote> listVendas = loteDAO.ListarDesativadosPorCategoria(categoria);

            ProdutoDAO produtoDAO = new ProdutoDAO();
            CategoriaDAO categoriaDAO = new CategoriaDAO();

            foreach (var item in listVendas)
            {
                item.Produto = produtoDAO.ListarSelecionadoID(item.Produto.ID);
                item.Categoria = categoriaDAO.ListarSelecionadoID(item.Categoria.ID);
            }

            return View(listVendas);
        }
    }
}