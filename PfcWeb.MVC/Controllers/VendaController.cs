﻿using PFC.Data.DAO;
using PfcWeb.Data.DAO;
using PfcWeb.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;
using System.Web.Services;
using System.Web.UI;

namespace PfcWeb.MVC.Controllers
{
    [Authorize(Roles = "CAIXA")]
    public class VendaController : Controller
    {
        public ActionResult Venda()
        {
            HttpCookie cookie = Request.Cookies.Get("UsuarioID");
            VendaDAO vendaDAO = new VendaDAO();
            Venda venda = new Venda();
            venda.DataHora = DateTime.Now;
            venda.Status = "NÃO FINALIZADO";
            venda.Usuario = new Usuario();
            venda.Usuario.ID = Convert.ToInt32(cookie.Value);

            vendaDAO.CadastrarVenda(venda);

            return View();
        }

        [HttpPost]
        public JsonResult AdicionarProduto(Lote obj)
        {
            HttpCookie cookie = Request.Cookies.Get("UsuarioID");

            VendaDAO vendaDAO = new VendaDAO();
            ItemVenda itemVenda = new ItemVenda();

            itemVenda.Venda = new Venda();

            itemVenda.Venda.ID = vendaDAO.GetLastIdVenda(Convert.ToInt32(cookie.Value));

            ProdutoDAO produtoDAO = new ProdutoDAO();

            LoteController loteController = new LoteController();
            Lote loteSelecionado = loteController.GetLoteByBarcode(obj.Barcode);

            if (loteSelecionado != null)
            {
                loteSelecionado.Produto = produtoDAO.ListarSelecionadoID(loteSelecionado.Produto.ID);

                if (loteSelecionado.ID != 0)
                {
                    itemVenda.Quantidade = Convert.ToInt32(obj.QuantidadeItens);
                    itemVenda.Lote = new Lote();
                    itemVenda.Lote = loteSelecionado;

                    PrateleiraDAO prateleiraDAO = new PrateleiraDAO();

                    Prateleira prateleira = prateleiraDAO.ListarSelecionadoIDLote(loteSelecionado.ID);

                    if (prateleira != null)
                    {
                        if (obj.QuantidadeItens <= prateleira.Quantidade)
                        {
                            ItemVendaDAO itemVendaDAO = new ItemVendaDAO();

                            var QuantidadeAtual = prateleira.Quantidade - obj.QuantidadeItens;
                            prateleiraDAO.AtualizarQuantidade(prateleira.ID, QuantidadeAtual);
                            itemVendaDAO.Cadastrar(itemVenda);

                            if (loteSelecionado.DataValidade < DateTime.Now)
                            {
                                return Json(new
                                {
                                    produtoID = 0,
                                    produtoNome = "ERRO VALIDADE",
                                    produtoValor = 0
                                });
                            }
                            else if (loteSelecionado.DataValidade.AddDays(-31) < DateTime.Now)
                            {
                                loteSelecionado.Produto.ValorUnitario = loteSelecionado.Produto.ValorUnitario - (loteSelecionado.Produto.ValorUnitario * 0.2);

                                return Json(new
                                {
                                    produtoID = loteSelecionado.Produto.ID,
                                    produtoNome = loteSelecionado.Produto.Nome,
                                    produtoValor = loteSelecionado.Produto.ValorUnitario
                                });
                            }
                            else
                            {
                                return Json(new
                                {
                                    produtoID = loteSelecionado.Produto.ID,
                                    produtoNome = loteSelecionado.Produto.Nome,
                                    produtoValor = loteSelecionado.Produto.ValorUnitario
                                });
                            }
                        }
                        else
                        {
                            var quantidade = Convert.ToInt32(obj.QuantidadeItens) - prateleira.Quantidade;
                            return Json(new
                            {
                                produtoID = 0,
                                produtoNome = "ERRO QUANTIDADE",
                                produtoValor = 0,
                                produtoQuantidade = quantidade
                            });
                        }
                    }
                    else
                    {
                        return Json(new
                        {
                            produtoID = 0,
                            produtoNome = "ERRO PRATELEIRA",
                            produtoValor = 0,
                            produtoQuantidade = 0
                        });
                    }

                }
                else
                {
                    return Json(new
                    {
                        produtoID = 0,
                        produtoNome = 0,
                        produtoValor = 0
                    });
                }
            }
            else
            {
                return Json(new
                {
                    produtoID = 0,
                    produtoNome = 0,
                    produtoValor = 0
                });
            }

        }

        [HttpPost]
        public JsonResult FinalizarCompra(string TotalItens, string ValorTotal, string ValorRecebido, string Troco)
        {
            HttpCookie cookie = Request.Cookies.Get("UsuarioID");

            VendaDAO vendaDAO = new VendaDAO();

            int lastID = vendaDAO.GetLastIdVenda(Convert.ToInt32(cookie.Value));

            Venda venda = new Venda();

            venda.Status = "FINALIZADO";
            venda.TotalItens = Convert.ToInt32(TotalItens);
            venda.ValorTotal = Convert.ToDouble(ValorTotal);
            venda.Recebido = Convert.ToDouble(ValorRecebido);
            venda.Troco = Convert.ToDouble(Troco);

            VendaDAO dao = new VendaDAO();
            if (dao.FinalizarVenda(venda, lastID))
            {
                return Json(new
                {
                    mensagem = "SUCESSO"
                });
            }
            else
            {
                return Json(new
                {
                    mensagem = "ERRO"
                });
            }
        }
        
        public ActionResult ListaVenda()
        {
            VendaDAO dao = new VendaDAO();

            return View(dao.ListarTodos());
        }

        public void ControleDeVendasNaoFinalizada()
        {
            VendaDAO vendaDAO = new VendaDAO();

            List<Venda> listVenda = vendaDAO.GetVendasNaoFinalizadas();

            foreach (var item in listVenda)
            {
                ItemVendaDAO itemVendaDAO = new ItemVendaDAO();

                List<ItemVenda> listItemVenda = itemVendaDAO.ListarPorIDVenda(item);

                foreach (var itemVenda in listItemVenda)
                {
                    LoteDAO loteDAO = new LoteDAO();

                    itemVenda.Venda = item;

                    itemVenda.Lote = loteDAO.ListarSelecionadoID(itemVenda.Lote.ID);

                    itemVenda.Lote.QuantidadeItens = itemVenda.Lote.QuantidadeItens + itemVenda.Quantidade;

                    if (loteDAO.AtualizarQuantidade(itemVenda.Lote.ID, itemVenda.Lote.QuantidadeItens))
                    {
                        itemVendaDAO.DeletarItemVendaNaoFinalizado(itemVenda);
                    }
                }

                vendaDAO.DeletarVendaNaoFinalizada(item);
            }
        }
    }
}