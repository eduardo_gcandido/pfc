﻿using PfcWeb.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;

namespace PfcWeb.MVC.Controllers
{
    public class MenuController : Controller
    {
        [Authorize(Roles = "CAIXA")]
        public ActionResult MenuFuncCaixa()
        {
            return View();
        }

        [Authorize(Roles = "ESTOQUE")]
        public ActionResult MenuFuncEstoque()
        {
            return View();
        }

        [Authorize(Roles = "GERENTE")]
        public ActionResult MenuGerente()
        {
            return View();
        }
    }
}