﻿using PfcWeb.Data.DAO;
using PfcWeb.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace PfcWeb.MVC.Controllers
{
    [Authorize(Roles = "GERENTE")]
    public class FuncionarioController : Controller
    {
        public ActionResult CadastrarFuncionario()
        {

            DepartamentoDAO departamentoDAO = new DepartamentoDAO();

            var funcionario = new Funcionario();

            ViewBag.DepartamentoID = departamentoDAO.ListarTodos();

            return View(funcionario);
        }

        [HttpPost]
        public ActionResult CadastrarFuncionario(Funcionario funcionario)
        {
            FuncionarioDAO dao = new FuncionarioDAO();

            var funcionarioSelecionado = dao.ListarSelecionado(funcionario.CPF);

            if (funcionarioSelecionado.CPF == null)
            {
                EnderecoDAO enderecoDAO = new EnderecoDAO();

                if (enderecoDAO.Cadastrar(funcionario.Endereco))
                {
                    funcionario.Endereco.ID = enderecoDAO.ListarUltimo().ID;

                    if (dao.Cadastrar(funcionario))
                    {
                        return RedirectToAction("MenuGerente", "Menu");
                    }
                }
            }
            else
            {
                TempData["Message"] = string.Format("Funcionario já cadastrado");
            }
            return View();
        }
        
        public ActionResult ListaFuncionario()
        {
            FuncionarioDAO dao = new FuncionarioDAO();

            List<Funcionario> listFuncionario = dao.ListarTodos();

            DepartamentoDAO departamentoDAO = new DepartamentoDAO();

            foreach (var item in listFuncionario)
            {
                item.Departamento = departamentoDAO.ListarSelecionado(item.Departamento.ID);
            }

            return View(listFuncionario);
        }
        
        public ActionResult EditarFuncionario(string cpf)
        {
            if (cpf == null)
            {
                return RedirectToAction("ListaFuncionario", "Funcionario");
            }
            else
            {
                FuncionarioDAO dao = new FuncionarioDAO();

                Funcionario funcionario = dao.ListarSelecionado(cpf);

                EnderecoDAO enderecoDAO = new EnderecoDAO();

                DepartamentoDAO departamentoDAO = new DepartamentoDAO();

                ViewBag.Departamento = departamentoDAO.ListarTodos();

                funcionario.Endereco = enderecoDAO.ListarSelecionado(funcionario.Endereco.ID);

                return View(funcionario);
            }
        }
        
        [HttpPost]
        public ActionResult EditarFuncionario(Funcionario funcionario)
        {
            FuncionarioDAO dao = new FuncionarioDAO();

            if (dao.AtualizarDados(funcionario))
            {
                return RedirectToAction("ListaFuncionario", "Funcionario");
            }
            else
            {
                TempData["Message"] = string.Format("Erro ao efetuar a atualização");
                return View(funcionario);
            }
        }
        
        public ActionResult DemitirFuncionario(string cpf)
        {
            FuncionarioDAO dao = new FuncionarioDAO();

            dao.Demitir(cpf);

            return RedirectToAction("ListaFuncionario", "Funcionario");

        }
        
        public ActionResult ReadmitirFuncionario(string cpf)
        {
            FuncionarioDAO dao = new FuncionarioDAO();

            dao.Readmitir(cpf);

            return RedirectToAction("ListaFuncionario", "Funcionario");

        }
    }
}