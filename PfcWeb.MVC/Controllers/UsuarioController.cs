﻿using PfcWeb.Data.DAO;
using PfcWeb.Domain.Entities;
using PfcWeb.Domain.Enum;
using PfcWeb.MVC.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;

namespace PfcWeb.MVC.Controllers
{
    public class UsuarioController : Controller
    {
        // GET: Login
        public ActionResult Login()
        {
            return View();
        }

        [AllowAnonymous]
        public ActionResult Error()
        {
            return View();
        }

        [HttpPost]
        [AllowAnonymous]
        public ActionResult Login(Usuario user)
        {
            if (ModelState.IsValid)
            {
                string senha = user.Senha;

                UsuarioDAO dao = new UsuarioDAO();
                Usuario usuarioRecebido = new Usuario();
                usuarioRecebido = dao.EfetuarLogin(user.Login);
                if (usuarioRecebido != null)
                {
                    if (usuarioRecebido.Senha.Equals(senha))
                    {
                        switch (usuarioRecebido.NivelAcesso)
                        {
                            case EnumNivelAcesso.GERENTE:
                                CreateAuthorizeTicket(usuarioRecebido);
                                Response.Cookies.Add(new HttpCookie("UsuarioID", usuarioRecebido.ID.ToString()));
                                return RedirectToAction("MenuGerente", "Menu");

                            case EnumNivelAcesso.CAIXA:
                                CreateAuthorizeTicket(usuarioRecebido);
                                Response.Cookies.Add(new HttpCookie("UsuarioID", usuarioRecebido.ID.ToString()));
                                return RedirectToAction("MenuFuncCaixa", "Menu");

                            case EnumNivelAcesso.ESTOQUE:
                                CreateAuthorizeTicket(usuarioRecebido);
                                Response.Cookies.Add(new HttpCookie("UsuarioID", usuarioRecebido.ID.ToString()));
                                return RedirectToAction("MenuFuncEstoque", "Menu");
                        }
                    }
                    else
                    {
                        ModelState.AddModelError("Senha", "Senha incorreta");
                    }
                }
                else
                {
                    ModelState.AddModelError("Login", "Usuario não encontrado");
                }
            }

            return View(user);
        }

        [Authorize]
        public ActionResult Logout()
        {
            FormsAuthentication.SignOut();

            Response.Cookies[FormsAuthentication.FormsCookieName].Expires = DateTime.Now.AddYears(-1);

            return RedirectToAction("Login", "Usuario");
        }

        [Authorize(Roles = "GERENTE")]
        public ActionResult CadastrarUsuario()
        {
            var user = new Usuario();
            return View(user);
        }

        [Authorize(Roles = "GERENTE")]
        [HttpPost]
        public ActionResult CadastrarUsuario(Usuario user)
        {
            bool validadeOk = true;

            if (user.Login == null)
            {
                ModelState.AddModelError("Usuario", "Usuario não pode ser nulo");
                validadeOk = false;
            }
            if (user.Senha == null)
            {
                ModelState.AddModelError("Senha", "Senha não pode ser nulo");
                validadeOk = false;
            }
            if (user.NivelAcesso.ToString() == null)
            {
                ModelState.AddModelError("NivelAcesso", "Nivel de Acesso não pode ser nulo");
                validadeOk = false;
            }
            if (validadeOk)
            {
                UsuarioDAO dao = new UsuarioDAO();
                if (dao.Cadastrar(user))
                {
                    return RedirectToAction("ListaUsuario", "Usuario");
                }
            }

            return View();
        }

        [Authorize(Roles = "GERENTE")]
        public ActionResult EditarUsuario(int id)
        {
            if (id == 0)
            {
                return RedirectToAction("ListaUsuario", "Usuario");
            }
            else
            {
                UsuarioDAO dao = new UsuarioDAO();

                Usuario usuario = dao.ListarSelecionado(id);

                return View(usuario);
            }
        }

        [Authorize(Roles = "GERENTE")]
        [HttpPost]
        public ActionResult EditarUsuario(Usuario usuario)
        {
            UsuarioDAO dao = new UsuarioDAO();

            if (dao.AtualizarDados(usuario))
            {
                return RedirectToAction("ListaUsuario", "Usuario");
            }
            else
            {
                TempData["Message"] = string.Format("Erro ao efetuar a atualização");
                return View(usuario);
            }
        }

        [Authorize(Roles = "GERENTE")]
        public ActionResult DesativarUsuario(int id)
        {
            if (id == 0)
            {
                return RedirectToAction("ListaUsuario", "Usuario");
            }
            else
            {
                UsuarioDAO dao = new UsuarioDAO();

                if (dao.Desativar(id))
                {
                    TempData["Message"] = string.Format("Usuário desativado com sucesso");

                    return RedirectToAction("ListaUsuario", "Usuario");
                }
                else
                {
                    TempData["Message"] = string.Format("Erro ao efetuar a desativação");

                    return RedirectToAction("ListaUsuario", "Usuario");
                }

            }
        }

        [Authorize(Roles = "GERENTE")]
        public ActionResult AtivarUsuario(int id)
        {
            if (id == 0)
            {
                return RedirectToAction("ListaUsuario", "Usuario");
            }
            else
            {
                UsuarioDAO dao = new UsuarioDAO();

                if (dao.Ativar(id))
                {
                    TempData["Message"] = string.Format("Usuário Ativado com sucesso");

                    return RedirectToAction("ListaUsuario", "Usuario");
                }
                else
                {
                    TempData["Message"] = string.Format("Erro ao efetuar a desativação");

                    return RedirectToAction("ListaUsuario", "Usuario");
                }

            }
        }

        [Authorize(Roles = "GERENTE")]
        public ActionResult ListaUsuario()
        {
            UsuarioDAO dao = new UsuarioDAO();

            List<Usuario> list = dao.ListarTodos();

            List<UsuarioViewModel> listViewModel = new List<UsuarioViewModel>();

            foreach (var item in list)
            {
                UsuarioViewModel obj = new UsuarioViewModel();

                obj.ID = item.ID;
                obj.Login = item.Login;
                obj.Senha = item.Senha;
                obj.NivelAcesso = item.NivelAcesso.ToString();
                if (item.Status == true)
                {
                    obj.Status = "Ativado";
                }
                else
                {
                    obj.Status = "Desativado";
                }

                listViewModel.Add(obj);
            }

            return View(listViewModel);
        }

        public void CreateAuthorizeTicket(Usuario usuario)
        {

            FormsAuthenticationTicket authTicket = new FormsAuthenticationTicket(
              1,
              usuario.Login.ToString(),  // Id do usuário é muito importante
              DateTime.Now,
              DateTime.Now.AddMinutes(30),  // validade 30 min tá bom demais
              false,   // Se você deixar true, o cookie ficará no PC do usuário
              usuario.NivelAcesso.ToString());

            HttpCookie cookie = new HttpCookie(FormsAuthentication.FormsCookieName, FormsAuthentication.Encrypt(authTicket));

            Response.Cookies.Add(cookie);
        }


    }
}