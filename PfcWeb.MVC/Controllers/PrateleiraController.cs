﻿using PFC.Data.DAO;
using PfcWeb.Data.DAO;
using PfcWeb.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Mail;
using System.Web;
using System.Web.Mvc;

namespace PfcWeb.MVC.Controllers
{
    [Authorize(Roles = "ESTOQUE")]
    public class PrateleiraController : Controller
    {
        public ActionResult CadastrarPrateleira()
        {
            Prateleira prateleira = new Prateleira();

            LoteDAO loteDAO = new LoteDAO();

            ViewBag.Lote = loteDAO.ListarTodos();
            
            if (ViewBag.Lote.Count > 0)
            {
                return View(prateleira);
            }
            else
            {
                TempData["Message"] = string.Format("Não existem lotes cadastrados, favor cadastrar");
                return RedirectToAction("ListaPrateleira", "Prateleira");
            }

        }

        [HttpPost]
        public ActionResult CadastrarPrateleira(Prateleira prateleira)
        {
            PrateleiraDAO dao = new PrateleiraDAO();

            if (dao.Cadastrar(prateleira))
            {
                LoteDAO loteDAO = new LoteDAO();

                Lote lote = loteDAO.ListarSelecionadoID(prateleira.Lote.ID);

                int quantidadeNova = lote.QuantidadeItens - prateleira.Quantidade;

                loteDAO.AtualizarQuantidade(lote.ID, quantidadeNova);

                return RedirectToAction("ListaPrateleira", "Prateleira");
            }

            return View(prateleira);
        }
        
        public ActionResult ListaPrateleira()
        {
            PrateleiraDAO prateleiraDAO = new PrateleiraDAO();

            List<Prateleira> list = new List<Prateleira>();

            list = prateleiraDAO.ListarTodos();

            LoteDAO loteDAO = new LoteDAO();

            foreach (var item in list)
            {
                item.Lote = loteDAO.ListarSelecionadoID(item.Lote.ID);
            }

            return View(list);
        }
        
        public ActionResult EditarPrateleira(int id)
        {
            if (id == 0)
            {
                return RedirectToAction("ListaPrateleira", "Prateleira");
            }
            else
            {
                PrateleiraDAO dao = new PrateleiraDAO();

                Prateleira prateleira = new Prateleira();

                prateleira = dao.ListarSelecionadoID(id);

                LoteDAO loteDAO = new LoteDAO();

                ViewBag.Lote = loteDAO.ListarTodos();

                if (ViewBag.Lote.Count > 0)
                {
                    return View(prateleira);
                }
                else
                {
                    TempData["Message"] = string.Format("Não existem lotes cadastrados, favor cadastrar");
                    return RedirectToAction("ListaPrateleira", "Prateleira");
                }
            }
        }
        
        [HttpPost]
        public ActionResult EditarPrateleira(Prateleira prateleira)
        {
            PrateleiraDAO dao = new PrateleiraDAO();

            if (dao.AtualizarDados(prateleira))
            {
                return RedirectToAction("ListaPrateleira", "Prateleira");
            }
            else
            {
                TempData["Message"] = string.Format("Erro ao efetuar a atualização");
                return View(prateleira);
            }
        }

        public void EnviaEmailReporPrateleira()
        {
            PrateleiraDAO prateleiraDAO = new PrateleiraDAO();

            List<Prateleira> listPrateleira = prateleiraDAO.ListarQuaseAcabando();

            if (listPrateleira != null)
            {
                FuncionarioDAO funcionarioDAO = new FuncionarioDAO();

                List<Funcionario> listFuncionario = funcionarioDAO.ListarfuncionarioEstoque();

                if (listFuncionario != null)
                {
                    foreach (var item in listFuncionario)
                    {
                        string prateleiraId = "";

                        foreach (var prateleira in listPrateleira)
                        {
                            prateleiraId = prateleiraId + prateleira.ID + ",";
                        }

                        if (prateleiraId.Length > 0)
                        {
                            prateleiraId = prateleiraId.Remove(prateleiraId.Length - 1);
                        }

                        if (listPrateleira.Count > 0)
                        {
                            Email mail = new Email()
                            {
                                Sender = "prime.stock.tcc@gmail.com",
                                To = item.Email,
                                subject = "Itens prateleira chegando ao fim",
                                Body = "Favor repor as prateleiras com os ID's " + prateleiraId + " pois os itens estão chegando ao fim."
                            };

                            try
                            {
                                //Instância smtp do servidor, neste caso o gmail.
                                SmtpClient smtp = new SmtpClient();
                                smtp.Host = "smtp.gmail.com";
                                smtp.Port = 587;
                                smtp.UseDefaultCredentials = false;
                                smtp.Credentials = new System.Net.NetworkCredential
                                ("prime.stock.tcc@gmail.com", "PrimeStock.2018");// Login e senha do e-mail.
                                smtp.EnableSsl = true;
                                smtp.Send(mail.Sender, mail.To, mail.subject, mail.Body);
                            }
                            catch (Exception ex)
                            {
                                throw new Exception(ex.Message);
                            }
                        }
                    }
                }
            }
        }
    }
}