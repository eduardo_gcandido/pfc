﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PfcWeb.Domain.Enum
{
    public enum EnumNivelAcesso
    {
        GERENTE,
        CAIXA,
        ESTOQUE
    }
}