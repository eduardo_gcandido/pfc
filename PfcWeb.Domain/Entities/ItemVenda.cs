﻿using PfcWeb.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PfcWeb.Domain.Entities
{
    public class ItemVenda
    {
        public int ID { get; set; }
        public Venda Venda { get; set; }
        public Lote Lote { get; set; }
        public int Quantidade { get; set; }
    }
}