﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PfcWeb.Domain.Entities
{
    public class Categoria
    {
        public int ID { get; set; }
        public string Descricao { get; set; }
    }
}
