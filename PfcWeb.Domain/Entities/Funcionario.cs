﻿using PfcWeb.Entities.Enum;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PfcWeb.Domain.Entities
{
    public class Funcionario
    {
        public string Nome { get; set; }
        public string CPF { get; set; }
        public string RG { get; set; }
        public char Sexo { get; set; }
        public string Email { get; set; }
        public long Telefone { get; set; }
        public EnumCargo Cargo { get; set; }
        public DateTime DataAdmissao { get; set; }
        public DateTime? DataDemissao { get; set; }
        public Endereco Endereco { get; set; }
        public Departamento Departamento { get; set; }
    }
}