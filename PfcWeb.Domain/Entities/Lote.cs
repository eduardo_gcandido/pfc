﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PfcWeb.Domain.Entities
{
    public class Lote
    {
        public int ID { get; set; }
        public int QuantidadeItens { get; set; }
        public DateTime DataEntrada { get; set; }
        public DateTime DataValidade { get; set; }
        public Categoria Categoria { get; set; }
        public Produto Produto { get; set; }
        public string Descricao { get; set; }
        public string Barcode { get; set; }
        public bool Status { get; set; }
    }
}