﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PfcWeb.Domain.Entities
{
    public class Produto
    {
        public int ID { get; set; }
        public string Nome { get; set; }
        public double ValorUnitario { get; set; }
    }
}