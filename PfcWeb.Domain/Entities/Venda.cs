﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PfcWeb.Domain.Entities
{
    public class Venda
    {
        public int ID { get; set; }
        public DateTime DataHora { get; set; }
        public Usuario Usuario { get; set; }
        public int TotalItens { get; set; }
        public double ValorTotal { get; set; }
        public double Recebido { get; set; }
        public double Troco { get; set; }
        public string Status { get; set; }
    }
}