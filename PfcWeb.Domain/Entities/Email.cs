﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PfcWeb.Domain.Entities
{
    public class Email
    {
        public string To { get; set; }
        public string Sender { get; set; }
        public string subject { get; set; }
        public string Body { get; set; }
    }
}
