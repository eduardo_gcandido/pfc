﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PfcWeb.Domain.Entities
{
    public class Prateleira
    {
        public int ID { get; set; }
        public string Descricao { get; set; }
        public int Quantidade { get; set; }
        public Lote Lote { get; set; }
    }
}
